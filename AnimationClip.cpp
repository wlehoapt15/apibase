#include "AnimationClip.h"
#include "GameObject.h"

AnimationClip::AnimationClip(GameObject* Obj)
{
	m_Progress		= 0.f;
	m_AnimationTime = 0.f;
	m_FrameCnt		= 0;
	m_LoopCnt		= 0;
	m_DirectionCnt  = 0;
	m_NowDirection  = 0;
	m_IsLoop		= false;
	m_IsPlay		= false;
	m_HasExitTime   = false;
	m_GameObject	= Obj;
}

AnimationClip::~AnimationClip()
{

}

void AnimationClip::Init()
{

}

void AnimationClip::Update(float dt)
{
	if (m_IsPlay == false)
	{
		if (m_HasExitTime == true)
		{
			if (m_Progress <= dt)
			{
				// 애니메이션이 끝난경우
				m_Progress = 0.f;
				m_LoopCnt = 0;
				return;
			}
		}
		// HasExiTtime이 아닌경우
		else
		{
			m_Progress = 0.f;
			m_LoopCnt = 0;
			return;
		}
	}
	// 애니메이션 진행
	m_Progress += dt / m_AnimationTime;
	if (m_Progress >= 1.0f)
	{
		m_Progress = 0.f;
		m_LoopCnt++;
	}
	// 루프여부 체크
	if (m_IsLoop == false && m_LoopCnt >= 1)
	{
		m_IsPlay  = false;
	}
}

void AnimationClip::SetFrameCnt(int Cnt)
{

}
void AnimationClip::SetDirectionCnt(int Cnt)
{

}

void AnimationClip::Play()
{
	m_IsPlay = true;
}

void AnimationClip::Stop()
{
	m_IsPlay = false;
}