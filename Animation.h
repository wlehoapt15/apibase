#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "Component.h"

class AnimationClip;

class Animation : public Component
{
public:
	Animation();
	~Animation();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

public:
	// 애니메이션 시작
	void Play();
	// 애니메이션 시작
	void Play( AnimationClip* Clip );
	// 애니메이션 멈춤
	void Stop();
	// 애니메이션 중인지 확인
	bool IsPlaying();
	// 애니메이션 클립 설정
	void SetAnimationClip(AnimationClip* Clip);
	AnimationClip* GetAnimationClip();
private:
	// 현재 애니메이션 클립
	AnimationClip* m_NowAnimationClip;
};

#endif
