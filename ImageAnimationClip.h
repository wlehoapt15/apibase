#ifndef _IMAGEANIMATION_H_
#define _IMAGEANIMATION_H_

#include "AnimationClip.h"
#include "Renderer.h"
#include "Image.h"

class ImageAnimationClip : public AnimationClip
{
public:
	ImageAnimationClip(GameObject* Obj);
	~ImageAnimationClip();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void SetFrameCnt(int Cnt);
	virtual void SetDirectionCnt(int Cnt);
	virtual void Play();
	void		 SetImage(Image* image);

private:
	Renderer*	 m_Renderer;
	int			 m_ImageWidth;
	int			 m_ImageHeight;
	int			 m_ImageWidthOffSet;
	int			 m_ImageHeightOffSet;
};

#endif
