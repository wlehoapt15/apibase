#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#include "Vector2.h"

struct Rect
{
	Rect(float Left, float Top, float Right, float Bottom);
	Rect(Vector2 lefttop, Vector2 rightbottom);


	Vector2 LeftTop;
	Vector2 RightBottom;

};

struct Sphere
{
	Vector2 Center;
	float	Radius;
	Sphere(Vector2 center, float radius);
	Sphere(float x, float y, float radius);
};

namespace Physics
{
	bool RectToPointCollisionCheck	  (Rect rect, Vector2 Point);
	bool RectToRectCollisionCheck	  (Rect rect1, Rect rect2);
	bool SphereToPointCollisionCheck  (Sphere sphere, Vector2 Point);
	bool SphereToSphereCollisionCheck (Sphere sphere1, Sphere sphere2);
	bool RectToSphereCollisionCheck	  (Rect rect, Sphere sphere);
}


#endif