#ifndef _EDITMAP_OBJECT_H_
#define _EDITMAP_OBJECT_H_

#include "GameObject.h"

class EditMapObject : public GameObject
{
public:
	EditMapObject();
	~EditMapObject();

public:
	virtual void Init();
	virtual void Release();

public:

private:
};

#endif