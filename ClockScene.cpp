#include "ClockScene.h"
#include <iostream>
#include <math.h>
#include "Vector2.h"
#include "SceneManager.h"
#include "MainScene.h"

ClockScene::ClockScene()
{

}

ClockScene::~ClockScene()
{

}

void ClockScene::Init()
{
	//GetSystemTime(&m_NowTime);	// 태평양 표준시
	GetLocalTime(&m_NowTime);
}

void ClockScene::Update(float dt)
{
	//GetSystemTime(&m_NowTime);	// 태평양 표준시
	GetLocalTime(&m_NowTime);
}

void ClockScene::Render(HDC hdc)
{
	char str[255];
	sprintf(str, "%d시 %d분 %d초", m_NowTime.wHour, m_NowTime.wMinute, m_NowTime.wSecond );
	TextOut(hdc, 0, 580, str, strlen(str));

	static int X = 400;				// 원의 중점
	static int Y = 300;				// 원의 중점
	static float Radius = 150.f;    // 원의 반지름

	
	for (int i = 0; i < 12; i++)
	{
		sprintf(str, "%d", i + 1);
		TextOut(hdc, X + Radius * cos(ANGLE_TO_RADIAN( i * 30 - 60 ) ),
					 Y + Radius * sin(ANGLE_TO_RADIAN( i * 30 - 60 ) ), str, strlen(str));
	}

	// 초침 그리기
	// 펜생성
	HPEN Pen	= CreatePen(PS_SOLID, 5, RGB(255, 0, 0));
	HPEN OldPen = (HPEN)SelectObject(hdc, Pen);
	MoveToEx(hdc, X, Y, NULL);		// 초침의 시작점
	LineTo(hdc, X + (Radius * 0.8f) * cos(ANGLE_TO_RADIAN(m_NowTime.wSecond * 6 - 90)), // 초침의 끝점
				Y + (Radius * 0.8f) * sin(ANGLE_TO_RADIAN(m_NowTime.wSecond * 6 - 90)));
	DeleteObject(SelectObject(hdc, OldPen));

	// 펜생성
	Pen = CreatePen(PS_SOLID, 10, RGB(0, 255, 0));
	OldPen = (HPEN)SelectObject(hdc, Pen);

	float MinuteAngle = m_NowTime.wMinute * 6 - 90;
	// 분침 그리기
	MoveToEx(hdc, X, Y, NULL);		// 분침의 시작점
	LineTo(hdc, X + (Radius * 0.6f) * cos(ANGLE_TO_RADIAN(MinuteAngle)), // 분침의 끝점
				Y + (Radius * 0.6f) * sin(ANGLE_TO_RADIAN(MinuteAngle)));
	
	DeleteObject(SelectObject(hdc, OldPen));

	// 시침 그리기
	Pen = CreatePen(PS_SOLID, 15, RGB(0, 0, 255));
	OldPen = (HPEN)SelectObject(hdc, Pen);

	m_NowTime.wHour = m_NowTime.wHour % 12;
	float HourAngle = ( m_NowTime.wHour * 30 - 90 ) + ( MinuteAngle / 5.f );

	MoveToEx(hdc, X, Y, NULL);		// 시침의 시작점
	LineTo(hdc, X + (Radius * 0.4f) * cos(ANGLE_TO_RADIAN(HourAngle)), // 시침 끝점
				Y + (Radius * 0.4f) * sin(ANGLE_TO_RADIAN(HourAngle)));


	DeleteObject(SelectObject(hdc, OldPen));
}

void ClockScene::Release()
{

}

LRESULT ClockScene::SceneProc(HWND hWnd, UINT iMessage,WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_ESCAPE:
				{
					SCENE_MGR->ChangeScene<MainScene>();
				}
				break;
			}
			break;
		}
	}
	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}