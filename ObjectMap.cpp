#include "ObjectMap.h"
#include "MapScroll.h"
#include "RunnerMap.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"

ObjectMap::ObjectMap()
{

}

ObjectMap::~ObjectMap()
{

}

void ObjectMap::Init()
{
	GameObject::Init();
	Renderer* renderer = AddComponent<Renderer>();
	//renderer->SetImage(Image::GetImage("map.bmp"));
	RunnerMap* runner = AddComponent<RunnerMap>();
	//runner->m
	m_Transform->SetAnchorPoint(Vector2(0.5f, 0.5f));
	SetName("ObjectMap");
}

void ObjectMap::Release()
{
	
}