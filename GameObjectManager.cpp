#include "GameObjectManager.h"
#include "GameObject.h"
#include "Transform.h"
#include "Physics.h"
#include "Input.h"
#include "Renderer.h"
#include "Image.h"
#include "UiObject.h"

GameObjectManager* GameObjectManager::m_Instance = NULL;

GameObjectManager::GameObjectManager()
{

}
GameObjectManager::~GameObjectManager()
{

}

void GameObjectManager::Update(float dt)
{
	for (int i = 0; i < m_GameObjectList.size(); i++)
	{
		if (m_GameObjectList[i]->GetActive() == false) continue;
		m_GameObjectList[i]->Update(dt);
	}
	UICollisionCheck(dt);
}

void GameObjectManager::UICollisionCheck(float dt)
{
	/*for (int i = 0; i < m_GameObjectList.size(); i++)*/
	for (int i = 0; i < m_UIObjectList.size(); i++)
	{
		// buttonObject가 아니면 생략
		//if (typeid(UiObject) != typeid(*m_GameObjectList[i])) continue;
		//Transform* transform	= m_GameObjectList[i]->GetComponent<Transform>();
		//Renderer*  renderer		= m_GameObjectList[i]->GetComponent<Renderer>();
		Transform* transform = m_UIObjectList[i]->GetComponent<Transform>();
		Renderer*  renderer = m_UIObjectList[i]->GetComponent<Renderer>();

		// 충돌체크할 rect 구하기
		Vector2	   lefttop		 = transform->GetPosition();
		Vector2	   rightbottom   = Vector2( lefttop.GetX() + renderer->GetImage()->GetWidth(),
											lefttop.GetY() + renderer->GetImage()->GetHeight());

		//UiObject* uiObject = dynamic_cast<UiObject*>(m_GameObjectList[i]);
		UiObject* uiObject = dynamic_cast<UiObject*>(m_UIObjectList[i]);
		if (uiObject == nullptr)
			continue;
		if (Physics::RectToPointCollisionCheck( Rect(lefttop, rightbottom),
												Input::GetMousePosition()))
		{
			// 버튼에 마우스포인터가 최초로 들어왔을 경우 enter
			if (uiObject->GetPointerEnter() == false)
			{
				uiObject->OnPointerEnter(dt);
				uiObject->SetPointerEnter( true );
			}
			// 이전에 들어온 경우가 있을경우 stay
			else
			{
				uiObject->OnPointerStay(dt);
			}
			// 마우스 왼쪽 버튼이 눌렸다면 
			if (Input::GetKey(VK_LBUTTON))
			{
				// 최초로 눌렸을 경우
				if (uiObject->GetPointerDown() == false)
				{
					uiObject->OnPointerDown(dt);
					uiObject->SetPointerDown( true );
				}
			}
			else
			{
				// 마우스 버튼이 뗘질 경우 
				if (uiObject->GetPointerDown() == true)
				{
					uiObject->OnPointerUp(dt);
					uiObject->SetPointerDown(false);
				}
			}
		}
		else
		{
			// 버튼에서 포인터가 나간경우
			if (uiObject->GetPointerEnter() == true)
			{
				uiObject->OnPointerExit(dt);
				uiObject->SetPointerDown(false);
				uiObject->SetPointerEnter(false);
			}
		}
	}
}

void GameObjectManager::Render(HDC hdc)
{
	for (int i = 0; i < m_GameObjectList.size(); i++)
	{
		if (m_GameObjectList[i]->GetActive() == false) continue;
		m_GameObjectList[i]->Render(hdc);
	}
}

void GameObjectManager::Release()
{
	for (int i = 0; i < m_GameObjectList.size(); i++)
	{
		if (m_GameObjectList[i] != NULL)
		{
			m_GameObjectList[i]->Release();
			delete m_GameObjectList[i];
		}
	}
	m_GameObjectList.clear();
}


void GameObjectManager::AddObject(GameObject* Obj, 
								  Vector2 Position, 
								  float	  Rotation )
{
	if (FindObject(Obj) == nullptr)
		Obj->Init();
	m_GameObjectList.push_back(Obj);
	Obj->GetComponent<Transform>()->SetPosition(Position);
	Obj->GetComponent<Transform>()->SetRotation(Rotation);
}

void GameObjectManager::AddUiObject(UiObject* Obj,
									Vector2 Position, 
									float	Rotation )
{
	if (FindObject(Obj) == nullptr)
		Obj->Init();
	m_UIObjectList.push_back(Obj);
	Obj->GetComponent<Transform>()->SetPosition(Position);
	Obj->GetComponent<Transform>()->SetRotation(Rotation);
}

void GameObjectManager::Destroy(GameObject* Obj)
{
	for (std::vector<GameObject*>::iterator iter 
		 = m_GameObjectList.begin();
		 iter != m_GameObjectList.end() ; iter++ )
	{
		if (*iter == Obj)
		{
			Obj->Release();
			delete Obj;
			m_GameObjectList.erase(iter);
			break;
		}
	}
}

GameObject* GameObjectManager::FindObject(GameObject* Obj)
{
	for (int i = 0; i < m_GameObjectList.size(); i++)
	{
		if (Obj == m_GameObjectList[i])
			return Obj;
	}
	return NULL;
}

GameObject* GameObjectManager::FindObject(char*		Name)
{
	for (int i = 0; i < m_GameObjectList.size(); i++)
	{
		if ( Name == m_GameObjectList[i]->GetName() )
			return m_GameObjectList[i];
	}
	return NULL;
}