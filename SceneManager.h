#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_

///////////////////////////////////////////////////////
// 씬 클래스
// 작성자	: 
// 작성날짜	: 2017.05.09
// 용도		: 씬의 관리, 씬의 변경 담당 클래스
///////////////////////////////////////////////////////
#include <Windows.h>
#include "Scene.h"
#include "GameObjectManager.h"
#define SCENE_MGR SceneManager::GetInstance()

class SceneManager
{
private:				// 싱글턴 클래스
	SceneManager();
	~SceneManager();
	SceneManager(const SceneManager& Val);				 // 정의 X
	SceneManager& operator = (const SceneManager& Val);  // 정의 X
	static SceneManager* m_Instance;

public:
	static SceneManager* GetInstance()
	{
		if (m_Instance == nullptr) m_Instance = new SceneManager;
		return m_Instance;
	}
	
	template < typename SCENE_TYPE >
	void ChangeScene()
	{
		Scene* NextScene = dynamic_cast<Scene*>( new SCENE_TYPE );
		if (NULL == NextScene) return;
		if (NULL != m_NowScene)
		{
			m_NowScene->Release();
			OBJECT_MGR->Release();
			delete m_NowScene;
		}
		m_NowScene = NextScene;
		m_NowScene->Init();
	}

	void SetScene(Scene* scene) { m_NowScene = scene;  }

public:		// 씬 랜더, 갱신, 프로시저
	void	Update(float dt);
	void	Render(HDC hdc);
	LRESULT SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	Scene*		m_NowScene;
};

#endif