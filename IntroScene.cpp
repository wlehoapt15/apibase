#include "IntroScene.h"
#include "SceneManager.h"
#include "MainScene.h"
#include "GameManager.h"
#pragma comment (lib, "vfw32.lib")

IntroScene::IntroScene()
{

}
IntroScene::~IntroScene()
{

}

void IntroScene::Init()
{
	// MCI 재생 윈도우 생성
	m_hMCI = MCIWndCreate(GameManager::GetInstance()->GethWnd(),
		GameManager::GetInstance()->GethInstance(),
		MCIWNDF_NOTIFYANSI | MCIWNDF_NOMENU |
		MCIWNDF_NOTIFYALL | MCIWNDF_NOPLAYBAR,
		"Intro.wmv"
	);

	// MCI 윈도우 크기 조절
	SetWindowPos(m_hMCI, 0, 0, 0, GameManager::GetInstance()->GetRect().right
			, GameManager::GetInstance()->GetRect().bottom, SWP_NOZORDER | SWP_NOMOVE);

	// 마우스 커서 감춤
	ShowCursor(FALSE);
	SetFocus(GameManager::GetInstance()->GethWnd());
	GameManager::GetInstance()->SetMoviePlay(true);
	// 재생시작
	if (m_hMCI != NULL)
	{
		MCIWndPlay(m_hMCI);
		SetFocus(GameManager::GetInstance()->GethWnd());
	}
}

void IntroScene::Update(float dt)
{
	if (MCIWndGetPosition(m_hMCI) >= MCIWndGetEnd(m_hMCI))
	{
		MCIWndClose(m_hMCI);
		MCIWndDestroy(m_hMCI);
		SCENE_MGR->ChangeScene<MainScene>(); 
		GameManager::GetInstance()->SetMoviePlay(false);
		ShowCursor(TRUE);
	}
}

void IntroScene::Render(HDC hdc)
{

}

void IntroScene::Release()
{

}

 LRESULT IntroScene::SceneProc(HWND hWnd, UINT iMessage,
	 WPARAM wParam, LPARAM lParam)
 {
	 switch (iMessage)
	 {
		case WM_KEYDOWN:
		{
			if (m_hMCI != NULL)
			{
				MCIWndClose(m_hMCI);
				MCIWndDestroy(m_hMCI);
				SCENE_MGR->ChangeScene<MainScene>();
				GameManager::GetInstance()->SetMoviePlay(false);

				ShowCursor(TRUE);
			}
			break;
		}



	 }
	 return DefWindowProc(hWnd, iMessage, wParam, lParam);
 }