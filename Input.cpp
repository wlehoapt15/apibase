#include "Input.h"
#include "GameManager.h"

namespace Input
{
	bool IsFocus		= false;			// 포커스 상태
	bool KeyDown[255]	= { false, };		// 키눌림 확인용 배열
	
	/*void Update() {
		while (m_KeyDownList.empty()))
		{
			KeyDown(m_KeyDownL)
		}
	}*/

	// 윈도우 포커스 설정 함수
	void SetFocusWindow(bool Focus)
	{
		IsFocus = Focus;
	}

	// 키보드( 마우스 버튼 ) 눌리는중인지 확인
	bool GetKey(int VK_KEY)
	{
		if (IsFocus == true)
		{
			if (GetAsyncKeyState(VK_KEY))
			{
				return true;
			}
		}
		return false;
	}

	// 키보드( 마우스 버튼 ) 한번 눌렸는지 확인 
	bool GetKeyDown(int VK_KEY)
	{
		if (IsFocus == true)
		{
			if (GetAsyncKeyState(VK_KEY))
			{
				if (KeyDown[VK_KEY] == false)	// 전에 키가 눌리지 않았다면
				{
					KeyDown[VK_KEY] = true;		// 눌린 상태로 변경
					return true;				// 참 반환
				}
			}
			else								// 키가 눌리지 않고있다.
			{
				KeyDown[VK_KEY] = false;		// 눌리지 않는 상태로 변경
			}
		}
		return false;
	}

	// 키보드( 마우스 버튼 ) 땔때 확인 
	bool GetKeyUp(int VK_KEY)
	{
		if (IsFocus == true)
		{
			if (GetAsyncKeyState(VK_KEY))
			{
				KeyDown[VK_KEY] = true;				// 눌림상태만 체크
			}
			else
			{
				if (KeyDown[VK_KEY] == true)		// 직전에 눌린 상황이었다면
				{
					KeyDown[VK_KEY] = false;		// 눌리지않은상태로 체크
					return true;					// 참반환
				}
			}
		}
		return false;
	}

	// 마우스 좌표 얻어오기
	Vector2 GetMousePosition()
	{
		POINT MousePosition;
		// GetCursorPos -> 전체영역의 마우스 좌표 얻기
		GetCursorPos(&MousePosition);
		// ScreenToClient -> 전체영역의 좌표를 클라이언트 영역으로 변환
		ScreenToClient(GameManager::GetInstance()->GethWnd(), &MousePosition);
		return Vector2(MousePosition.x, MousePosition.y);
	}

};