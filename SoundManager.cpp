#include "SoundManager.h"

SoundManager* SoundManager::m_Instance = NULL;
SoundManager::SoundManager()
{
}
SoundManager::~SoundManager()
{
}

bool	SoundManager::Init(int ChannelCnt)
{
	m_ChannelCnt = ChannelCnt;
	// 시스템 인스턴스 생성
	FMOD::System_Create(&m_System);
	if (m_System == NULL) return false;
	m_System->init(ChannelCnt, FMOD_INIT_NORMAL, NULL);
	m_ChannelList = new SoundChannelInfo[m_ChannelCnt];
	return true;
}

void	SoundManager::SoundInit(char* SoundName, bool IsBGM, bool IsLoop)
{
	// 이미 등록된 사운드인지 확인
	std::map<std::string, FMOD::Sound* >::iterator SoundIter =
		m_SoundList.find( std::string(SoundName) );
	// 사운드가 이미 존재하면
	if (SoundIter != m_SoundList.end()) return;
	// 사운드 불러오기
	FMOD::Sound* NewSound = NULL;
	if (IsBGM == true)		// 배경음이면
	{
		// BGM은 스트리밍 방식의 사운드로 로드한다.
		m_System->createStream( SoundName, IsLoop ? 
								FMOD_LOOP_NORMAL : FMOD_DEFAULT, NULL, &NewSound);
	}
	else // 효과음이면
	{
		m_System->createSound( SoundName, IsLoop ? 
							   FMOD_LOOP_NORMAL : FMOD_DEFAULT, NULL, &NewSound);
	}
	// 사운드 등록
	m_SoundList.insert(std::make_pair(std::string(SoundName), NewSound));
}

int		SoundManager::SoundPlay(char* SoundName, float Volume)
{
	std::map< std::string, FMOD::Sound* >::iterator SoundIter 
		= m_SoundList.find( std::string(SoundName) );
	// 재생할 사운드가 없다.
	if (SoundIter == m_SoundList.end()) { return -1;  }

	bool IsPlay = false;
	// 빈채널 찾기
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		m_ChannelList[i].m_Channel->isPlaying(&IsPlay);
		if (IsPlay == false)
		{
			// 채널에 사운드를 할당함과 동시에 재생
			m_System->playSound( FMOD_CHANNEL_FREE, SoundIter->second, false,
								 &(m_ChannelList[i].m_Channel ));
			m_ChannelList[i].m_Channel->setVolume(Volume);				// 재생볼륨 설정
			m_ChannelList[i].SetPlayingSound(SoundIter->first.data() );	// 채널정보에 재생중인 사운드 설정
			return i;
		}
	}
	return -1;
}
void	SoundManager::SoundStop(char* SoundName)
{
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i] == SoundName)
		{
			m_ChannelList[i].m_Channel->stop();
		}
	}
}

void	 SoundManager::SoundStop(int ChannelIndex)
{
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return;
	m_ChannelList[ChannelIndex].m_Channel->stop();
}

void	SoundManager::SetSoundVolume(char* SoundName, float Volume)
{
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i] == SoundName)
		{
			m_ChannelList[i].m_Channel->setVolume(Volume);
		}
	}
}

void	SoundManager::SetSoundVolume(int ChannelIndex, float Volume)
{
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return;
	m_ChannelList[ChannelIndex].m_Channel->setVolume(Volume);
}

void	SoundManager::SoundPause(char* SoundName)
{
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i] == SoundName)
		{
			m_ChannelList[i].m_Channel->setPaused(true);
		}
	}
}

void	SoundManager::SoundPause(int ChannelIndex)
{
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return;
	m_ChannelList[ChannelIndex].m_Channel->setPaused(true);
}

void	SoundManager::SoundResume(char* SoundName)
{
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i] == SoundName)
		{
			m_ChannelList[i].m_Channel->setPaused(false);
		}
	}
}

void	SoundManager::SoundResume(int ChannelIndex)
{
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return;
	m_ChannelList[ChannelIndex].m_Channel->setPaused(false);
}

bool	SoundManager::IsPlaying(char* SoundName)
{
	bool IsPlay;
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i].m_PlayingSound == SoundName)
		{
			m_ChannelList[i].m_Channel->isPlaying(&IsPlay);
			if (IsPlay == true) return true;
		}
	}
	return false;
}

bool	SoundManager::IsPlaying(int ChannelIndex)
{
	bool IsPlay;
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return false;
	m_ChannelList[ChannelIndex].m_Channel->isPlaying(&IsPlay);
	return IsPlay;
}

bool	SoundManager::IsPause(char* SoundName)
{
	bool IsPaused;
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if (m_ChannelList[i].m_PlayingSound == SoundName)
		{
			m_ChannelList[i].m_Channel->getPaused(&IsPaused);
			if (IsPaused == true)  return true;
		}
	}
	return false;
}

bool	SoundManager::IsPause(int ChannelIndex)
{
	bool IsPaused;
	if (ChannelIndex < 0 || ChannelIndex >= m_ChannelCnt) return false;
	m_ChannelList[ChannelIndex].m_Channel->getPaused(&IsPaused);
	return IsPaused;
}

void	SoundManager::Update()
{
	if (m_System != NULL)
		m_System->update();
}

void	SoundManager::Release()
{
	// 채널 정리
	for (int i = 0; i < m_ChannelCnt; i++)
	{
		if( m_ChannelList[i].m_Channel != NULL )
			m_ChannelList[i].m_Channel->stop();
	}
	delete []m_ChannelList;
	m_ChannelCnt = 0;

	// 사운드 정리
	std::map< std::string, FMOD::Sound* >::iterator SoundIter = m_SoundList.begin();
	for (; SoundIter != m_SoundList.end(); SoundIter++)
	{
		SoundIter->second->release();
	}
	m_SoundList.clear();

	// 시스템 정리
	if (m_System != NULL)
	{
		m_System->release();
		m_System->close();
		m_System = NULL;
	}
}