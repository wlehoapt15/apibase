#ifndef _TIMEMANAGER_H_
#define _TIMEMANAGER_H_

///////////////////////////////////////////////////////
// 타임 매니저 클래스 
// 작성자	: 
// 작성날짜	: 2017.05.09
// 용도		: 프레임 간격 ( DeltaTime ) 계산, 
//			  월드시간 측정, FPS 계산
//////////////////////////////////////////////////////

#include <Windows.h>
#define TIME_MGR TimeManager::GetInstance()

class TimeManager
{
	// 생성자, 소멸자 ( 싱글턴 )
private:
	TimeManager();
	~TimeManager();
	TimeManager(const TimeManager& Val);				// 정의하지 않음
	TimeManager& operator = (const TimeManager& Val);	// 정의하지 않음
	static TimeManager* m_Instance;

public:
	static TimeManager* GetInstance()
	{
		if (m_Instance == nullptr) m_Instance = new TimeManager;
		return m_Instance;
	}
public:		// 맴버 함수
	void	Update();			// 타임 계산
	float	GetFPS();			// FPS  반환
	float	GetWorldTime() { return m_WorldTime; }		// WorldTime 반환
	float	GetDeltaTime() { return m_DeltaTime; }		// DeltaTime 반환

private:	// 맴버 변수
	
	BOOL			m_IsHardwareAble;	// 하드웨어 지원 여부
	LARGE_INTEGER	m_Frequency;		// 하드웨어 정밀도를 가진다
	LARGE_INTEGER	m_CheckTime;		// 시간측정용 변수
	LONGLONG		m_BeginTime;		// 시간 측정 시작 시간
	LONGLONG		m_EndTime;			// 시간 측정 끝	시간
	float			m_WorldTime;
	float			m_DeltaTime;
};


#endif
