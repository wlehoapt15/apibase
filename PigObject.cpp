#include "PigObject.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "Animation.h"
#include "PigMoveScript.h"

PigObject::PigObject() 
{

}

PigObject::~PigObject() 
{

}

void PigObject::Init()
{
	AddComponent<Renderer>();
	AddComponent<Animation>();
	AddComponent<PigMoveScript>();

	m_Transform->SetAnchorPoint(Vector2(0.5f, 0.5f));
}

void PigObject::Release() 
{

}