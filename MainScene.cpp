#include "SceneManager.h"
#include "MainScene.h"
#include "ClockScene.h"
#include "BitmapScene.h"
#include "TransformScene.h"
#include "HorizontalScene.h"
#include "MapEditScene.h"
#include "GameObjectManager.h"
#include "UiObject.h"

MainScene::MainScene() 
{
	PointerDown = false;
}

MainScene::~MainScene()
{

}

void MainScene::Init()
{
	/*GameObject* Obj = new UiObject;
 	hBit = bit.LoadFile("BACKGROUND.bmp");
	OBJECT_MGR->AddObject(Obj, Vector2( 300.f, 300.f ) );*/
}

void MainScene::Update(float dt)
{
	//OBJECT_MGR->Update(dt);
}

void MainScene::Render(HDC hdc)
{
	//bit.FixelRender(hdc);
	bit.Render(hdc);
	OBJECT_MGR->Render(hdc);
}

void MainScene::Release()
{
	OBJECT_MGR->Release();
}

LRESULT MainScene::SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case '1':
					SCENE_MGR->ChangeScene<ClockScene>();
					break;
				case '2':
					SCENE_MGR->ChangeScene<BitmapScene>();
					break;
				case '3':
					SCENE_MGR->ChangeScene<TransformScene>();
					break;
				case '5':
					SCENE_MGR->ChangeScene<HorizontalScene>();
					break; 
				case '6':
					SCENE_MGR->ChangeScene<MapEditScene>();
					break;
			}
		}
		case WM_LBUTTONUP:
		{
			PointerDown = false;
			break;
		}

		case WM_MOUSEMOVE:
		{
			if (PointerDown == true)
			{
				POINT pt;
				pt .x = LOWORD(lParam);
				pt .y = HIWORD(lParam);
				bit.Effect(pt, 50);
			}
			break;
		}
		case WM_LBUTTONDOWN:
		{
			PointerDown = true;

			POINT pt;
			pt.x = LOWORD(lParam);
			pt.y = HIWORD(lParam);
			bit.Effect(pt, 50);
			break;
		}
	}
	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}