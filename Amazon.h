#ifndef _AMAZON_H_

#define _AMAZON_H_

#include "GameObject.h"

class Amazon : public GameObject
{
public:
	Amazon();
	~Amazon();

public:
	virtual void Init();
	void	Update(float dt);
	void	Render(HDC hdc);
	virtual void Release();

};

#endif // _AMAZON_H_
