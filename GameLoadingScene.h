#ifndef _GAMELOADINGSCENE_H_
#define _GAMELOADINGSCENE_H_

#include "LoadingScene.h"
#include "GameObject.h"
#include "Renderer.h"
#include "Image.h"
#include "SceneManager.h"
#include <windows.h>
template <typename TYPE>
class GameLoadingScene : public LoadingScene<TYPE>
{
public:
	GameLoadingScene()
	{}
	~GameLoadingScene()
	{}
	
	// 순수 가상함수들
public:
	virtual void Init()
	{
		m_BackGround = new GameObject;
		m_BackGround->AddComponent<Renderer>()->SetImage(Image::GetImage("./Resource/배경/Back.bmp"));
		m_LoadSceneAsync.Progress = 0.f;
		StartLoadingThread();
	}
	virtual void Update(float dt)
	{
		if (m_LoadSceneAsync.Progress < 1.f)
		{
			m_LoadSceneAsync.Progress += dt / 3.0f;
		}
		else
		{
			m_LoadSceneAsync.Progress = 1.f;
			// 씬로드 완료까지 대기 ( 쓰레드가 종료될때까지 )
			DWORD ExitCode;
			GetExitCodeThread(m_ThreadHandle, &ExitCode);
			if (ExitCode == EXIT_SUCCESS)	// 쓰레드가 정상종료됨
			{
				// 씬전환
				SCENE_MGR->SetScene(m_LoadSceneAsync.NextScene);
				//SOUND_MGR->SoundPlay("BGM.mp3");
				Release();
			}
		}
	}
	virtual void Render(HDC hdc)
	{
		m_BackGround->Render(hdc);
		static float x = 600;
		static float y = 50;
		Rectangle(hdc, 100, 500, 100 + x * m_LoadSceneAsync.Progress, 500 + y);
	}

	virtual void Release() 
	{
		m_BackGround->Release();
		delete m_BackGround;
	}

private:
	GameObject* m_BackGround;
	HWND		m_hMCI;
};


#endif