#include "SceneManager.h"
#include "TransformScene.h"
#include "MainScene.h"
#include "GameManager.h"
#include "Renderer.h"
#include "Input.h"
#include "Transform.h"

XFORM GetWorldTransform(Vector2 Pos, Vector2 Scale, float Radian)
{
	XFORM Result;
	Result.eM11 = cos(Radian);
	Result.eM12 = sin(Radian);
	Result.eM21 = -sin(Radian);
	Result.eM22 =  cos(Radian);
	Result.eDx = Pos.GetX();
	Result.eDy = Pos.GetY();
	return Result;
}

Vector2 operator *(XFORM xform, Vector2 Vertex)
{
	Vector2 Result = Vertex;
	Result.SetX(xform.eM11 * Vertex.GetX() + xform.eM12 * Vertex.GetY() + xform.eDx * 1.f);
	Result.SetY(xform.eM21 * Vertex.GetX() + xform.eM22 * Vertex.GetY() + xform.eDy * 1.f);
	return Result;
}

TransformScene::TransformScene()
{

}

TransformScene::~TransformScene()
{

}

void TransformScene::Init()
{
	m_Scale		= Vector2(1.0f, 1.0f);
	m_Position  = Vector2(0.f, 0.f); 
	m_Rotation  = 0.f;

	m_LineStart = Vector2(0.f, 0.f);
	m_LineEnd = Vector2(100.f, 100.f);

	m_Missile = new GameObject;
	Renderer* MissileRender = m_Missile->AddComponent<Renderer>();
	MissileRender->SetImage(Image::GetImage("Missile.bmp"));
	MissileRender->SetAlpha(0.5f);
	MissileRender->GetComponent<Transform>()->SetAnchorPoint(Vector2(0.5, 0.5) );

	m_Image = new GameObject;
	Renderer* BackRenderer= m_Image->AddComponent<Renderer>();
	BackRenderer->SetImage(Image::GetImage("Back.bmp"));

	HDC hdc = GetDC( GameManager::GetInstance()->GethWnd() );
	
	SetGraphicsMode(hdc, GM_ADVANCED);	// 고급 그리기 모드로 설정
	
	ReleaseDC(GameManager::GetInstance()->GethWnd(), hdc);
}

void TransformScene::Update(float dt)
{
	if (Input::GetKey('A'))
	{
		m_Position.SetX(m_Position.GetX() - dt * 100.f);
	}

	if (Input::GetKey('D'))
	{
		m_Position.SetX(m_Position.GetX() + dt * 100.f);
	}

	if (Input::GetKey('S'))
	{
		m_Position.SetY(m_Position.GetY() + dt * 100.f);
	}

	if (Input::GetKey('W'))
	{
		m_Position.SetY(m_Position.GetY() - dt * 100.f);
	}

	if (Input::GetKey(VK_UP))
	{
		m_Scale.SetX( m_Scale.GetX() + 5.f * dt);
		m_Scale.SetY( m_Scale.GetY() + 5.f * dt);
	}

	if (Input::GetKey(VK_DOWN))
	{
		m_Scale.SetX(m_Scale.GetX() - 5.f * dt);
		m_Scale.SetY(m_Scale.GetY() - 5.f * dt);
	}

	if (Input::GetKey(VK_LEFT))
	{
		m_Rotation -= 3.141592f * dt;
	}

	if (Input::GetKey(VK_RIGHT))
	{
		m_Rotation += 3.141592f * dt;
	}
	// 월드변환행렬 구하기
	m_TransformMatrix = GetWorldTransform(m_Position, m_Scale, m_Rotation);
	Transform* transform = m_Missile->GetComponent<Transform>();
	transform->SetPosition(m_Position);
	transform->SetRotation(m_Rotation);
	transform->Update(dt);

}

void TransformScene::Render(HDC hdc)
{
	char TransformSTR[255];
	sprintf(TransformSTR, "Pos X : %g, Pos Y : %g", m_Position.GetX(), m_Position.GetY());
	TextOut(hdc, 0, 45, TransformSTR, strlen(TransformSTR));
	sprintf(TransformSTR, "Scale X : %g, Scale Y : %g", m_Scale.GetX(), m_Scale.GetY());
	TextOut(hdc, 0, 60, TransformSTR, strlen(TransformSTR));
	sprintf(TransformSTR, "Rot : %g", RADIAN_TO_ANGLE(m_Rotation) );
	TextOut(hdc, 0, 75, TransformSTR, strlen(TransformSTR));
	
	// 정점에 대해 변환 ( 선의 시작점 )
	Vector2 LineVertex = m_TransformMatrix * ( m_LineStart * m_Scale.GetX() );
	MoveToEx(hdc, LineVertex.GetX(), LineVertex.GetY(), NULL);
	
	// 정점에 대해 변환 ( 선의 끝점 )
	LineVertex = m_TransformMatrix * ( m_LineEnd * m_Scale.GetY() );
	LineTo(hdc, LineVertex.GetX(), LineVertex.GetY());

	m_Image->Render(hdc);
	//  월드 변환 행렬로 이미지 출력
	m_Missile->Render(hdc);
}

void TransformScene::Release()
{

}

LRESULT TransformScene::SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_ESCAPE:
				SCENE_MGR->ChangeScene<MainScene>();
				break;
			}
		}
	}
	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}