#ifndef _RUNNERMAP_H_
#define _RUNNERMAP_H_

#include "Component.h"
#include "Image.h"

class RunnerMap : public Component
{
public:
	RunnerMap();
	~RunnerMap();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

private:
	float ClientX;
	float ClientY;

	float curImageX;
	float curImageY;

	float MapSizeX;
	float MapSizeY;

	float Speed;

	Image* m_Image[];

	//std::vector<BITMAP> m_BackgroundList;
};

#endif