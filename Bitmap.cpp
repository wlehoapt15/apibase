#include "Bitmap.h"
#include "GameManager.h"
Bitmap::Bitmap()
{
	m_RasterData = NULL;
	m_hBit = NULL;
	ZeroMemory(&m_FileData, sizeof(m_FileData));
}

Bitmap::~Bitmap()
{

}

HBITMAP Bitmap::LoadFile(char* FileName)
{
	HANDLE File = NULL;
	DWORD  LoadData = 0;

	File = CreateFile(FileName, GENERIC_READ, 0, NULL, 
					OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (File == INVALID_HANDLE_VALUE)
	{
		return NULL;
	}

	// 비트맵 파일헤더 읽기
	ReadFile(File, &m_FileData.FileHeader, sizeof(BITMAPFILEHEADER), &LoadData, NULL);
	// 비트맵 인포헤더 읽기
	ReadFile(File, &m_FileData.InfoHeader, sizeof(BITMAPINFOHEADER), &LoadData, NULL);
	memcpy(&m_Info.bmiHeader, &m_FileData.InfoHeader, sizeof(BITMAPINFOHEADER));

	m_hBit = CreateDIBSection(NULL, &m_Info, DIB_RGB_COLORS, (void**)&m_RasterData, NULL, 0);
	//:: 데이터 읽어오기
	ReadFile(File, m_RasterData, sizeof(BYTE) * m_FileData.InfoHeader.biSizeImage, &LoadData, NULL);
	//:: 피치 계산
	m_FileData.Pitch =
		((m_FileData.InfoHeader.biWidth * (m_FileData.InfoHeader.biBitCount) >> 3) + 3) & ~3;

	m_FileData.Data = new BYTE[m_FileData.InfoHeader.biSizeImage];
	memcpy(m_FileData.Data, m_RasterData, m_FileData.InfoHeader.biSizeImage);

	CloseHandle(File);
	
	HDC hdc = GetDC(GameManager::GetInstance()->GethWnd());
	MemDC = CreateCompatibleDC(hdc);
	SelectObject(MemDC, m_hBit);
	ReleaseDC(GameManager::GetInstance()->GethWnd() , hdc );
	return m_hBit;
}

void	Bitmap::Render(HDC hdc)
{
	TransparentBlt(hdc, 0, 0, m_FileData.InfoHeader.biWidth, m_FileData.InfoHeader.biHeight
		, MemDC, 0, 0, m_FileData.InfoHeader.biWidth, m_FileData.InfoHeader.biHeight, RGB(255,0,255) );
}


void	Bitmap::FixelRender(HDC hdc)
{
	for (int x = 0; x < m_FileData.InfoHeader.biWidth; x++)
	{
		for (int y = 0; y < m_FileData.InfoHeader.biHeight; y++)
		{
			// 인덱스 계산
			int index = (y * m_FileData.Pitch) + (x * 3);
			SetPixel(hdc, y, x, RGB(m_RasterData[index + 2],
				m_RasterData[index + 1],
				m_RasterData[index + 0]));
		}
	}
}

void	Bitmap::Effect(POINT Pt, int Rad )
{
	FlipRaster();
	for (int y = Pt.y - Rad; y <  Pt.y + Rad; y++)
	{
		for (int x = Pt.x - Rad; x < Pt.x + Rad; x++)
		{
			// 인덱스 계산
			int index = (y * m_FileData.Pitch) + (x * 3);
			for (int i = 0; i <= Rad; i++)
			{
				if ((x - Pt.x) * (x - Pt.x) + (y - Pt.y) * (y - Pt.y) <= (i) * (i))
				{
					m_RasterData[index + 2] = 255;
					m_RasterData[index + 1] = 0;
					m_RasterData[index + 0] = 255;
					break;
						
				}
			}
		}
	}
	FlipRaster();
}
bool Bitmap::PixleCollision(POINT pt)
{
	int Index = (pt.y * m_FileData.Pitch) + (pt.x * 3);
	if (m_RasterData[Index + 0] == 255 &&
		m_RasterData[Index + 1] == 0 &&
		m_RasterData[Index + 2] == 255  )
	{
		return false;
	}
	return true;
}

void	Bitmap::FlipRaster()
{
	//:: 로딩이qa 되어있지 않으면 뒤집을수 없음
	if (m_RasterData == NULL)
	{
		return;
	}

	//:: 교체를 위한 임시 메모리할당
	BYTE	*temp = new BYTE[m_FileData.Pitch];

	//:: 교체
	for (int i = 0; i < m_FileData.InfoHeader.biHeight / 2; i++)
	{
		//< 위쪽과 아랫쪽을 바꾼다.
		int dest = i * m_FileData.Pitch;
		int src = (m_FileData.InfoHeader.biHeight - i - 1) * m_FileData.Pitch;
		memmove_s(temp, m_FileData.Pitch, &((BYTE*)m_RasterData)[dest], m_FileData.Pitch);
		memmove_s(&((BYTE*)m_RasterData)[dest], m_FileData.Pitch, &((BYTE*)m_RasterData)[src], m_FileData.Pitch);
		memmove_s(&((BYTE*)m_RasterData)[src], m_FileData.Pitch, temp, m_FileData.Pitch);
	}
	delete[] temp;
}

void	Bitmap::Restore()
{
	memmove_s(m_RasterData, m_FileData.InfoHeader.biSizeImage, m_FileData.Data, m_FileData.InfoHeader.biSizeImage);
	FlipRaster();
}
