#ifndef _SOUNDCHANNELINFO_H_
#define _SOUNDCHANNELINFO_H_

#include "FMOD/FMOD.hpp"
#include <String>
struct SoundChannelInfo
{
	SoundChannelInfo();


	FMOD::Channel*			m_Channel;		// 채널
	std::string				m_PlayingSound; // 이 채널에서 플레이 중인 사운드 이름

	void SetPlayingSound(const char* SoundName) { m_PlayingSound = SoundName; }
	std::string GetPlayingSound(void)	  { return m_PlayingSound;		}

	bool operator == (std::string SoundName);	// 채널간 사운드 비교 연산자
	bool operator == (SoundChannelInfo& Info);	// 채널간 사운드 비교 연산자
};

#endif
