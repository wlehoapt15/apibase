#ifndef _GAMEOBJECTMANAGER_H_
#define _GAMEOBJECTMANAGER_H_

#include "GameObject.h"
#include "Vector2.h"
#include <vector>

#define OBJECT_MGR GameObjectManager::GetInstance()

class UiObject;

class GameObjectManager
{
	// 싱글턴 클래스 작성
private:
	static GameObjectManager* m_Instance;

public:
	static GameObjectManager* GetInstance()
	{
		if (m_Instance == NULL) m_Instance = new GameObjectManager;
		return m_Instance;
	}

private:
	GameObjectManager();
	~GameObjectManager();

public:
	void Update(float dt);
	void UICollisionCheck(float dt);
	void Render(HDC hdc);
	void Release();

public:		// 오브젝트 추가 제거
	void AddObject( GameObject* Obj, 
					Vector2 Position = Vector2( 0.f , 0.f) , 
					float Rotation	 = 0.f );

	void AddUiObject( UiObject* Obj,
					  Vector2 Position	= Vector2(0.f, 0.f),
					  float Rotation	= 0.f);
	
	void Destroy(GameObject* Obj);

	GameObject* FindObject( GameObject* Obj );	// 오브젝트가 있는지 찾기
	GameObject* FindObject( char*		Name );	// 이름으로 찾기

private:
	std::vector<GameObject*> m_GameObjectList;
	std::vector<GameObject*> m_UIObjectList;
};

#endif