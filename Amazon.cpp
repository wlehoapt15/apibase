#include "Amazon.h"
#include "Renderer.h"
#include "Image.h"
#include "Animation.h"
#include "ImageAnimationClip.h"
#include "Transform.h"

Amazon::Amazon() {

}
Amazon::~Amazon() {

}

void Amazon::Init() {

	Renderer*	render	=	AddComponent<Renderer>();
	Animation*	ani		=	AddComponent<Animation>();
	render->SetImage(Image::GetImage("Idle.bmp"));

	ImageAnimationClip* idle = new ImageAnimationClip(this);
	idle->SetAnimationTime(1.f); // 하나의 애니메이션 재생시간
	idle->SetFrameCnt(16); // source image 가로길이 (가로로 쭉 읽으면서 진행한다.
	idle->SetDirectionCnt(16); // source image 세로 위치
	idle->SetExitTime(false);
	idle->SetLoop(true);
	idle->SetImage(Image::GetImage("Idle.bmp"));

	ani->Play(idle);
}
void Amazon::Release() {

}