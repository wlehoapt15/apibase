#include "ImageAnimationClip.h"
#include "GameObject.h"

ImageAnimationClip::ImageAnimationClip(GameObject* Obj)
	: AnimationClip( Obj )
{
	m_ImageWidth		= 0;
	m_ImageHeight		= 0;
	m_ImageHeightOffSet = 0;
	m_ImageWidthOffSet  = 0;
	m_Renderer			= Obj->GetComponent<Renderer>();
}
ImageAnimationClip::~ImageAnimationClip()
{

}
void ImageAnimationClip::Init()
{

}
void ImageAnimationClip::Update(float dt)
{
	AnimationClip::Update(dt);
	m_Renderer->SetOffSetX((int)(m_FrameCnt * m_Progress) * m_ImageWidthOffSet);
	m_Renderer->SetOffSetY((int)(m_NowDirection) * m_ImageHeightOffSet);
}
void ImageAnimationClip::SetFrameCnt(int Cnt)
{
	if (Cnt <= 0)  return;
	m_FrameCnt			= Cnt;
	m_ImageWidthOffSet  = m_ImageWidth / Cnt;
}

void ImageAnimationClip::SetDirectionCnt(int Cnt)
{
	if (Cnt <= 0)  return;
	m_DirectionCnt		 = Cnt;
	m_ImageHeightOffSet  = m_ImageHeight / Cnt;
}

void ImageAnimationClip::Play()
{
	// 부모의 Play() 호출
	AnimationClip::Play();
	m_Renderer->SetSizeX(m_ImageWidthOffSet);
	m_Renderer->SetSizeY(m_ImageHeightOffSet);
}

void ImageAnimationClip::SetImage(Image* image)
{
	m_ImageWidth	= image->GetWidth();
	m_ImageHeight   = image->GetHeight();
	SetFrameCnt(m_FrameCnt);
	SetDirectionCnt(m_DirectionCnt);
}