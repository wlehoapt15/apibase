#include "MapEditScene.h"
#include "EditMapObject.h"
#include "GameObjectManager.h"
#include "EditMapUiObject.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "LOG_MGR.h"

MapEditScene::MapEditScene()
{

}

MapEditScene::~MapEditScene()
{

}

void MapEditScene::Init()
{
	//m_Cursor = nullptr;
	m_CurImage = nullptr;
	isClicked = false;
	m_BackgroundObj = new EditMapObject;
	OBJECT_MGR->AddObject(m_BackgroundObj);

	float offset = 100.f;
	
	for (int i = 0; i < 5; ++i) 
	{
		UiObject* button = new EditMapUiObject;
		OBJECT_MGR->AddObject(button, Vector2(650.f, (float)(i + 1) * offset));
		OBJECT_MGR->AddUiObject(button, Vector2(650.f, (float)(i + 1) * offset));

		if (i % 2 == 0)
		{
			button->SetNormalImage(Image::GetImage("Block1.bmp"));
			button->SetOverImage(Image::GetImage("Block1.bmp"));
		}
		else
		{
			button->SetNormalImage(Image::GetImage("Block2.bmp"));
			button->SetOverImage(Image::GetImage("Block2.bmp"));
		}

		m_UiObjList.push_back(button);
	}
}

void MapEditScene::Update(float dt)
{
	for (int i = 0; i < m_UiObjList.size(); ++i)
	{
		if (m_UiObjList[i]->GetPointerDown())
		{
			Image* img = m_UiObjList[i]->GetComponent<Renderer>()->GetImage();
			m_CurImage = img;
			//m_Cursor = new EditMapUiObject;
			//m_Cursor->AddComponent<Renderer>();
			//m_Cursor->SetNormalImage(img);
			//m_Cursor->SetOverImage(img);
			//isButtonClick = true;
		}
	}
}

void MapEditScene::Render(HDC hdc)
{

}

void MapEditScene::Release()
{

}

LRESULT MapEditScene::SceneProc(HWND hWnd, UINT iMessage,
	WPARAM wParam, LPARAM lParam) {

	switch (iMessage)
	{
	case WM_LBUTTONDOWN:
		if (m_CurImage != nullptr && isClicked)
		{
			INIT_LOG(LOG_CONSOLE_FILE);
			ADD_LOG("add block pos : %d, %d", LOWORD(lParam), HIWORD(lParam));
			// ui object 말고 실제 게임 장애물 object 만들기..
			GameObject* block = new UiObject;
			OBJECT_MGR->AddObject(block, Vector2(LOWORD(lParam), HIWORD(lParam)));
			block->GetComponent<Renderer>()->SetImage(m_CurImage);
			block->GetComponent<Transform>()->SetParent(m_BackgroundObj->GetComponent<Transform>());
			//block->GetComponent<Transform>()->SetPosition(Vector2(LOWORD(lParam), HIWORD(lParam)));
			m_CurImage = nullptr;
		}
		else
		{
			isClicked = !isClicked;
		}
		break;
	//case WM_MOUSEMOVE:
	//	// 이미지가 null 이 아니라면 룰루 따라다니고
	//	if (isClicked && m_CurImage != nullptr)
	//	{
	//		m_Cursor->GetComponent<Transform>()->SetPosition(Vector2(LOWORD(lParam), HIWORD(lParam)));
	//	}
	//	/*rect.right = LOWORD(lParam);
	//	rect.bottom = HIWORD(lParam);*/
	//	break;
	//case WM_LBUTTONUP:
	//	// 이미지가 null 이 아니라면 해당 위치에 이미지 고정, add object?? nono data 작성.
	//	if (m_Cursor != nullptr)
	//	{
	//		INIT_LOG(LOG_CONSOLE_FILE);
	//		ADD_LOG("add block pos : %d, %d", LOWORD(lParam), HIWORD(lParam));
	//		m_Cursor->GetComponent<Transform>()->SetParent(m_BackgroundObj->GetComponent<Transform>());
	//		delete(m_Cursor);
	//	}

		//isClicked = false;

		break;
	}

	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}