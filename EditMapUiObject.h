#ifndef _EDITMAPUI_OBJECT_H
#define _EDITMAPUI_OBJECT_H

#include "UiObject.h"

class Image;

class EditMapUiObject : public UiObject
{
public:
	EditMapUiObject();
	~EditMapUiObject();
public:
	virtual void OnPointerDown(float dt);
	virtual void OnPointerUp(float dt);

private:
	bool isClick;
};

#endif // !_EDITMAPUI_OBJECT_H
