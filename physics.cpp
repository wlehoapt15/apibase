#include "Physics.h"

Rect::Rect(float Left, float Top, float Right, float Bottom)
{
	LeftTop		= Vector2(Left, Top);
	RightBottom = Vector2(Right, Bottom);
}

Rect::Rect(Vector2 lefttop, Vector2 rightbottom)
{
	LeftTop		= lefttop;
	RightBottom = rightbottom;
}

Sphere::Sphere(Vector2 center, float radius)
{
	Center = center;
	Radius = radius;
}

Sphere::Sphere(float x, float y, float radius)
{
	Center = Vector2( x, y );
	Radius = radius;
}

// 사각 , 점 충돌
bool Physics::RectToPointCollisionCheck(Rect rect, Vector2 Point)
{
	if (rect.LeftTop.GetX() < Point.GetX() && Point.GetX() < rect.RightBottom.GetX() &&
		rect.LeftTop.GetY() < Point.GetY() && Point.GetY() < rect.RightBottom.GetY()  )
		return true;

	return false;
}

// 사각, 사각 충돌
bool Physics::RectToRectCollisionCheck(Rect rect1, Rect rect2)
{
	Vector2 CheckPoint = rect1.LeftTop;

	// rect1의 꼭지점들이 rect2에 있는지 검사
	if (RectToPointCollisionCheck(rect2, CheckPoint))	// LeftTop
		return true;
	
	CheckPoint.SetY(rect1.RightBottom.GetY());			// LeftBottom
	if (RectToPointCollisionCheck(rect2, CheckPoint))	// LeftBottom
		return true;

	CheckPoint.SetX(rect1.RightBottom.GetX());			// RightBottom
	if (RectToPointCollisionCheck(rect2, CheckPoint))	// RightBottom
		return true;

	CheckPoint.SetY(rect1.LeftTop.GetY());				// RightTop
	if (RectToPointCollisionCheck(rect2, CheckPoint))	// RightTop
		return true;


	// rect2의 꼭지점들이 rect1에 있는지 검사
	CheckPoint = rect2.LeftTop;						    // LeftTop
	if (RectToPointCollisionCheck(rect1, CheckPoint))	// LeftTop
		return true;

	CheckPoint.SetY(rect2.RightBottom.GetY());			// LeftBottom
	if (RectToPointCollisionCheck(rect1, CheckPoint))	// LeftBottom
		return true;

	CheckPoint.SetX(rect2.RightBottom.GetX());			// RightBottom
	if (RectToPointCollisionCheck(rect1, CheckPoint))	// RightBottom
		return true;

	CheckPoint.SetY(rect2.LeftTop.GetY());				// RightTop
	if (RectToPointCollisionCheck(rect1, CheckPoint))	// RightTop
		return true;

	return false;
}

// 원, 점 충돌
bool Physics::SphereToPointCollisionCheck(Sphere sphere, Vector2 Point)
{
	float Distance = sphere.Center.GetDistance( Point );
	if (Distance < sphere.Radius)
	{
		return true;
	}
	return false;
}

// 원 , 원 충돌
bool Physics::SphereToSphereCollisionCheck(Sphere sphere1, Sphere sphere2)
{
	float Distance = sphere1.Center.GetDistance(sphere2.Center);
	if (Distance < sphere1.Radius + sphere2.Radius )
	{
		return true;
	}
	return false;
}

// 사각, 사각 충돌
bool Physics::RectToSphereCollisionCheck(Rect rect, Sphere sphere)
{
	// 원 <-> 사각형 꼭지점 충돌 검사
	Vector2 CheckPoint = rect.LeftTop;						// LeftTop
	if (SphereToPointCollisionCheck(sphere, CheckPoint))	// LeftTop
		return true;

	CheckPoint.SetX( rect.RightBottom.GetX() );				// RightTop
	if (SphereToPointCollisionCheck(sphere, CheckPoint))	// RightTop
		return true;

	CheckPoint.SetY(rect.RightBottom.GetY());				// RightBottom
	if (SphereToPointCollisionCheck(sphere, CheckPoint))	// RightBottom
		return true;

	CheckPoint.SetX(rect.LeftTop.GetX());					// LeftBottom
	if (SphereToPointCollisionCheck(sphere, CheckPoint))	// LeftBottom
		return true;

	// 원의 꼭지점 4개 구하기
	CheckPoint = sphere.Center;
	CheckPoint.SetX( sphere.Center.GetX() + sphere.Radius );

	// 원의 오른쪽 꼭지점
	if (RectToPointCollisionCheck(rect, CheckPoint))
		return true;

	// 원의 왼쪽 꼭지점
	CheckPoint.SetX(sphere.Center.GetX() - sphere.Radius);
	if (RectToPointCollisionCheck(rect, CheckPoint))
		return true;
	
	// 원의 아래 꼭지점
	CheckPoint = sphere.Center;
	CheckPoint.SetY(sphere.Center.GetY() + sphere.Radius);
	if (RectToPointCollisionCheck(rect, CheckPoint))
		return true;

	// 원의 위 꼭지점
	CheckPoint.SetY(sphere.Center.GetY() - sphere.Radius);
	if (RectToPointCollisionCheck(rect, CheckPoint))
		return true;

	return false;
}