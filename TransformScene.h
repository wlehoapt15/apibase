#ifndef _TRANSFORMSCENE_H_
#define _TRANSFORMSCENE_H_

#include "Scene.h"
#include "Vector2.h"
#include "Image.h"
#include "GameObject.h"
class TransformScene : public Scene
{
public:
	TransformScene();
	~TransformScene();

public:
	virtual void	Init();
	virtual void	Update(float dt);
	virtual void	Render(HDC hdc);
	virtual void	Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	Vector2 m_Scale;
	Vector2 m_Position;
	float	m_Rotation;

	Vector2	m_LineStart;
	Vector2 m_LineEnd;
	XFORM	m_TransformMatrix;
	
	GameObject*	m_Missile;
	GameObject* m_Image;
};


#endif