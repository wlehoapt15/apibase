#include "GameScene.h"
#include "GameObjectManager.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "ObjectMap.h"
#include "ObjectPlayer.h"
#include "SoundManager.h"
#include "Bitmap.h"
GameScene::GameScene()
{

}

GameScene::~GameScene()
{

}



Bitmap Map2;
void GameScene::Init()
{
	Map2.LoadFile("Field_011.bmp");
	SOUND_MGR->SoundInit("BGM.mp3", true, true);
	GameObject* Map	= new ObjectMap;
	OBJECT_MGR->AddObject(Map);
	
	GameObject* Player = new ObjectPlayer;
	Player->GetComponent<Transform>()->SetParent(Map->GetComponent<Transform>());
	OBJECT_MGR->AddObject( Player, Vector2(400.f, 300.f ) );
	SOUND_MGR->SoundPlay("BGM.mp3" , 0.1);
	
}


void GameScene::Update(float dt)
{
}

void GameScene::Render(HDC hdc)
{
	for (int i = 0; i < m_RectList.size(); i++)
	{
		Rectangle(hdc, m_RectList[i].left, m_RectList[i].top,
						m_RectList[i].right, m_RectList[i].bottom);
	}
	if (IsClick == true)
	{
		Rectangle(hdc, rect.left, rect.top,
			rect.right, rect.bottom);
	}
	Map2.Render(hdc);
}

void GameScene::ChangeStage(int Stage)
{
	char FileName[255];
	sprintf(FileName, "STAGE_%d.txt", Stage);
	FILE* fp = fopen(FileName, "r" );
	if (fp == NULL) return;

	int Count = 0;
	fscanf(fp, "%d", &Count);

	//for (int i = 0; i < Count; i++)
	//{
	//	GameObject* Obj = new GameObject;
	//	Map* map = Obj->AddComponent<Map>();
	//	float Left;
	//	float Top;
	//	float Right;
	//	float Bottom;
	//	fscanf(fp, "%f %f %f %f", &Left, &Top, &Right, &Bottom);
	//	Rect rect(Left, Top, Right, Bottom);
	//	map->SetRect(rect);
	//}

}
void GameScene::Release()
{
}

LRESULT GameScene::SceneProc(HWND hWnd, UINT iMessage,
	WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_LBUTTONDOWN:
		IsClick = true;
		rect.left = LOWORD(lParam);
		rect.top = HIWORD(lParam);
		rect.right = LOWORD(lParam);
		rect.bottom = HIWORD(lParam);
		break;
	case WM_MOUSEMOVE:
		if (IsClick == true) 
		{
			rect.right = LOWORD(lParam);
			rect.bottom = HIWORD(lParam);
		}
		break;
	case WM_LBUTTONUP:
		m_RectList.push_back(rect);
		IsClick = false;
		break;
	}
	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}
