#include "RunnerMap.h"
#include "Transform.h"
#include "Renderer.h";
#include "GameManager.h"
#include "Input.h"
#include "Image.h"

RunnerMap::RunnerMap()
{

}

RunnerMap::~RunnerMap()
{

}


void RunnerMap::Init()
{
	Speed = 100.f;
	Renderer* renderer = GetComponent<Renderer>();
	ClientX = GameManager::GetInstance()->GetRect().right;
	ClientY = GameManager::GetInstance()->GetRect().bottom;
	// 리스트로 수정 요망.
	m_Image[0] = Image::GetImage("background1.bmp");
	m_Image[1] = Image::GetImage("background1.bmp");

	renderer->SetImage(m_Image[0]);

	MapSizeX = renderer->GetImage()->GetWidth();
	MapSizeY = renderer->GetImage()->GetHeight();

}

void RunnerMap::Update(float dt)
{
	float PosX = m_Transform->GetPosition().GetX();
	float PosY = m_Transform->GetPosition().GetY();

	PosX -= dt * Speed;
	if (PosX <= -MapSizeX + ClientX)
	{
		PosX = -MapSizeX + ClientX;
	}

	m_Transform->SetPosition(Vector2(PosX, PosY));
}

void RunnerMap::Release()
{

}