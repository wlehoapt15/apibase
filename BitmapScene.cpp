#include "BitmapScene.h"
#include "MainScene.h"
#include "SceneManager.h"
#include "Image.h"
#include "Input.h"

BitmapScene::BitmapScene()
{
	m_BackGround = NULL;
}

BitmapScene::~BitmapScene()
{

}

void	BitmapScene::Init()
{
	m_BackGround = Image::GetImage("Back.bmp");
	m_Cursor = Image::GetImage("Cursor.bmp");
	m_Cursor->SetAlpha(127);
	m_FadeInOut = Image::GetImage("FadeInOut.bmp");
	ShowCursor(FALSE);
}

void	BitmapScene::Update(float dt)
{
	static float FadeDt = 0.f;
	FadeDt += ( dt * 0.2f );
	m_FadeInOut->SetAlpha( 255 * ( 1.0f - FadeDt ) );
	if (FadeDt >= 1.f)
	{
		FadeDt = 1.f;
	}
}

void	BitmapScene::Render(HDC hdc)
{
	m_BackGround->Render(hdc, 400, 300);
	
	//m_FadeInOut->Render(hdc, 400, 300);
	
	m_Cursor->Render(hdc, Input::GetMousePosition().GetX() + m_Cursor->GetWidth() / 2,
						  Input::GetMousePosition().GetY() + m_Cursor->GetHeight() / 2);
}

void	BitmapScene::Release()
{
	ShowCursor(TRUE);
}

LRESULT BitmapScene::SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_ESCAPE:
				SCENE_MGR->ChangeScene<MainScene>();
				break;
			}
		}
	}
	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}