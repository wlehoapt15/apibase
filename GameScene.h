#ifndef _GAMESCENE_H_
#define _GAMESCENE_H_

#include "Scene.h"
#include <vector>
class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();

public:
	virtual void Init();
	virtual void Update( float dt );
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
			WPARAM wParam, LPARAM lParam);

	void ChangeStage(int Stage);
private:
	std::vector<RECT> m_RectList;
	bool IsClick;
	RECT rect;
private:

};

#endif