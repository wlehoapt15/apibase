#ifndef _OBJECTMAP_H_
#define _OBJECTMAP_H_

#include "GameObject.h"
class ObjectMap : public GameObject
{
public:
	ObjectMap();
	~ObjectMap();

public:
	virtual void Init();
	virtual void Release();
};

#endif