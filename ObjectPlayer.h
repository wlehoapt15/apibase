#ifndef _OBJECTPLAYER_H_
#define _OBJECTPLAYER_H_

#include "GameObject.h"

class ObjectPlayer : public GameObject
{
public:
	ObjectPlayer();
	~ObjectPlayer();

public:
	virtual void Init();
	virtual void Release();
};

#endif
