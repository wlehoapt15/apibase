#ifndef _MAPEDIT_SCENE_H_
#define _MAPEDIT_SCENE_H_

#include "Scene.h"
#include <vector>

class UiObject;
class GameObject;
class Image;

class MapEditScene : public Scene
{
public:
	MapEditScene();
	virtual ~MapEditScene();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
		WPARAM wParam, LPARAM lParam);

private:
	GameObject* m_BackgroundObj;

	std::vector<UiObject*> m_UiObjList;
	std::vector<RECT> m_RectList;
	RECT rect;
	
	bool isClicked;
	UiObject* m_Cursor;
	Image* m_CurImage;

};



#endif // _HORIZONTALSCENE_H_
