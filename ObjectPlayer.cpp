#include "ObjectPlayer.h"
#include "Transform.h"
#include "Renderer.h"
#include "Animation.h"
#include "Player.h"
ObjectPlayer::ObjectPlayer()
{

}
ObjectPlayer::~ObjectPlayer()
{

}
void ObjectPlayer::Init()
{
	AddComponent<Renderer>();
	AddComponent<Animation>();
	AddComponent<Player>();

	m_Transform->SetAnchorPoint( Vector2(0.5f, 0.5f) );
}
void ObjectPlayer::Release()
{

}