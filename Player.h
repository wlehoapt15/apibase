#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Component.h"
#include "Animation.h"
#include "Image.h"
#include "Transform.h"
#include "ImageAnimationClip.h"
#include "Renderer.h"

enum STATE
{
	STATE_IDLE,
	STATE_RUN,
	STATE_END
};
class Player : public Component
{
public:
	Player();
	~Player();
public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();
	void ChangeState(STATE state);
	void UpdateState(float dt);
private:
	Animation*			m_Ani;				// 애니메이션 컴포넌트
	ImageAnimationClip* m_Clip[STATE_END];	// 애니메이션 클립
	Image*				m_Image[STATE_END];	// 애니메이션 이미지
	Image*				m_ItemImage[5];
	Renderer*			m_Renderer;			// 랜더러 컴포넌트
	Transform*			m_MapScroll;		// 맵의 Transform
	Vector2				m_Dest;	
	float				m_AniDir;
	float				m_PlayerSpeed;
	STATE				m_State;
};

#endif