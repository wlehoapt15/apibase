#include "GameManager.h"

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance,
					  LPSTR CmdLine, int CmdShow)
{
	return GameManager::GetInstance()->Run( "tkRunner", 800, 600 );
}