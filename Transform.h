#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "Vector2.h"
#include "Component.h"
#include <Windows.h>

class Transform : public Component
{
public:
	Transform();
	~Transform();

	virtual void Init();
	virtual void Release();
	virtual void Update(float dt);

public:
	Vector2		GetPosition();
	Vector2		GetlocalPosition();
	void		SetPosition(Vector2& Pos);
	void		SetlocalPosition(Vector2& Pos);

	Vector2		GetScale();
	Vector2		GetlocalScale();
	void		SetScale(Vector2& Scale );
	void		SetlocalScale(Vector2& Scale );

	float		GetRotation();
	float		GetlocalRotation();
	void		SetRotation( float Radian );
	void		SetlocalRotation(float Radian);

	Vector2		GetAnchorPoint();
	void		SetAnchorPoint(Vector2 Anchor );
public:
	void		SetParent(Transform* Parent);
	Transform*	GetParent();

public:
	void		SetWorldTransform(HDC hdc, int SizeX = 0, int SizeY = 0);
	void		SetRotationTransform(HDC hdc, Vector2 Size );
	void		SetTranslateTransform(HDC hdc, float OffSet);
	static void ResetWorldTransform(HDC hdc);

private:
	Vector2		 m_AnchorPoint;
	Vector2		 m_Position;
	Vector2		 m_localPosition;
	Vector2		 m_Scale;
	Vector2		 m_localScale;
	float		 m_Rotation;
	float		 m_localRotation;
	XFORM		 m_WorldMatrix;
	static XFORM m_Identity;
	Transform*   m_Parent;
};


#endif