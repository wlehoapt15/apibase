#ifndef _PIG_OBJECT_H_
#define _PIG_OBJECT_H_

#include "GameObject.h"
#include "Image.h"

class PigObject : public GameObject
{
public:
	PigObject();
	~PigObject();

public:
	virtual void Init();
	virtual void Release();

public:

private:

};

#endif