#ifndef _EDITMAPMOVE_SCRIPT_H_
#define _EDITMAPMOVE_SCRIPT_H_

#include "Component.h"

class EditMapMoveScript : public Component
{
public:
	EditMapMoveScript();
	~EditMapMoveScript();
public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

private:

};

#endif