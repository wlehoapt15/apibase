#ifndef _MAPSCROLL_H_
#define _MAPSCROLL_H_

#include "Component.h"

class MapScroll : public Component
{
public:
	MapScroll();
	~MapScroll();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

private:
	float ClientX;
	float ClientY;

	float MapSizeX;
	float MapSizeY;

	float Speed;
};

#endif