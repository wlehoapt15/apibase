#ifndef _BUTTONOBEJCT_H_
#define _BUTTONOBJECT_H_

#include "GameObject.h"

class Image;
class Renderer;

class UiObject : public GameObject
{
public:
	UiObject();
	~UiObject();

public:
	virtual void Init();
	virtual void Release();

	void SetNormalImage();
	void SetOverImage();

	virtual void OnPointerEnter(float dt);
	virtual void OnPointerStay(float dt);
	virtual void OnPointerExit(float dt);

	virtual void OnPointerDown(float dt);
	virtual void OnPointerUp(float dt);

	bool		GetPointerEnter()				{ return m_IsPointerEnter;		}
	void		SetPointerEnter(bool isEnter)	{ m_IsPointerEnter = isEnter;   }
	bool		GetPointerDown()				{ return m_IsPointerDown;		}
	void		SetPointerDown(bool isDown)		{ m_IsPointerDown = isDown;		}

	void SetNormalImage(Image* img);
	void SetOverImage(Image* img);
private:
	Image*		m_NormalImage;
	Image*		m_OverImage;
	Renderer*	m_Renderer;

	bool		m_IsPointerEnter;
	bool		m_IsPointerDown ;
};


#endif