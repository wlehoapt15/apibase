#include "LoadingScene.h"


DWORD WINAPI LoadingSceneThread(LPVOID Arg)
{
	LoadSceneAsync& Async = *((LoadSceneAsync*)Arg);
	Async.NextScene->Init();
	return 0;
}