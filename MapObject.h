#ifndef _MAP_OBJECT_H_
#define _MAP_OBJECT_H_

#include "GameObject.h"
#include "Image.h"

class MapObject : public GameObject
{
public:
	MapObject();
	~MapObject();

public:
	virtual void Init();
	virtual void Release();

public:
	void InitBackgroundImage();
	void InitBackgroundImage(Image);

private:
	Image m_BackgroundImg;
};

#endif