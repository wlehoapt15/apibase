#ifndef _BITMAP_H_
#define _BITMAP_H_
#include <Windows.h>

struct BITMAPFILEDATA
{
	BITMAPFILEHEADER FileHeader;
	BITMAPINFOHEADER InfoHeader;
	BYTE*			 Data;
	UINT			 Pitch;
	BITMAPFILEDATA()
	{
		ZeroMemory(this, sizeof(BITMAPFILEDATA));
	}
};

class Bitmap
{
public:
	Bitmap();
	~Bitmap();

public:
	HBITMAP LoadFile(char* FileName);
	void	Render(HDC hdc);
	void	FlipRaster();
	void	Restore();
	void	Effect(POINT Pt , int rad );
	void	FixelRender( HDC hdc );
	bool    PixleCollision(POINT pt);
private:
	BITMAPFILEDATA  m_FileData;
	BYTE*			m_RasterData;
	BITMAPINFO		m_Info;
	HBITMAP			m_hBit;
	HDC				MemDC;
};

#endif

