#include "Image.h"
#include "GameManager.h"
#include "Transform.h"

#include <map>
#include <string>

#pragma comment (lib, "msimg32.lib")

typedef std::map< std::string, Image* >			  IMAGE_LIST;		
typedef std::map< std::string, Image* >::iterator IMAGE_ITER;

HBITMAP		 Image::m_AlphaBit;	// 알파블랜드할 비트맵
HDC			 Image::m_AlphaDC;		// 알파블랜드할 DC
Vector2		 Image::m_AlphaSize;	// 이미지 가로세로 크기중 큰값
HBITMAP		 Image::m_InverseBit;
HDC			 Image::m_InverseDC;
IMAGE_LIST ImageList;

// 파일 이름으로 이미지 로딩 (없으면 생성)
Image*  Image::GetImage(char* FileName)
{
	// 기존에 등록된 Image 찾기.
	IMAGE_ITER FindResult = ImageList.find( FileName );
	// Image가 있다면.. ( find 함수결과 키에 해당하는 값이 없으면 end() 반복자를 반환 )
	if (FindResult != ImageList.end())
	{
		return FindResult->second;
	}
	// Image가 없다면. 새로 생성
	Image* NewImage = new Image(FileName);
	// map에 등록.. 하기.. 
	ImageList.insert(std::make_pair(std::string(FileName), NewImage));
	// Image 반환하기
	return NewImage;
}

// 현재 불러온 이미지 제거
void	Image::RemoveImage( char* FileName )
{
	IMAGE_ITER FindImage = ImageList.find(FileName);
	// 지울것이 있다면
	if (FindImage != ImageList.end())
	{
		delete FindImage->second;
		ImageList.erase(FindImage);
	}
}

// 현재 불러온 이미지 모두 제거
void	Image::Release()
{
	for (IMAGE_ITER iter = ImageList.begin(); iter != ImageList.end(); iter++)
	{
		delete iter->second;
	}
	ImageList.clear();
}

Image::Image()
{
	m_Width		= 0;
	m_Height	= 0;
	m_hBit		= NULL;
	m_MemDC		= NULL;
	m_AlphaDC	= NULL;
	m_AlphaBit	= NULL;
}

Image::Image(char* FileName)
{
	Init(FileName);
}

Image::~Image()
{
	if( m_hBit != NULL )
		DeleteObject(m_hBit);
	if (m_MemDC != NULL)
		DeleteDC(m_MemDC);
	if (m_AlphaDC != NULL)
		DeleteDC(m_AlphaDC);
	if (m_AlphaBit != NULL)
		DeleteObject(m_AlphaBit);
}
void Image::SetAlphaSize(Vector2 Size)
{
	m_AlphaSize = Size;
}
void Image::SetAlphaDC(HDC hdc)
{
	m_AlphaDC = hdc;
}
void Image::SetAlphaBitmap(HBITMAP bitmap)
{
	m_AlphaBit = bitmap;
}

void Image::Init(char* FileName)
{
	m_hBit = (HBITMAP)LoadImage(
						GetModuleHandle(NULL),
						FileName,
						IMAGE_BITMAP,
						0,
						0,
						LR_LOADFROMFILE);

	HDC hdc = GetDC(GameManager::GetInstance()->GethWnd());

	// 메모리 DC 만들기
	m_MemDC = CreateCompatibleDC(hdc);
	// 메모리 DC에 비트맵 선택
	SelectObject(m_MemDC, m_hBit);
	// 이미지 가로 세로 크기 구하기
	BITMAP Info;
	GetObject(m_hBit, sizeof(BITMAP), &Info);

	m_Width  = Info.bmWidth;
	m_Height = Info.bmHeight;
	
	// Blend 설정
	m_Blend.AlphaFormat			= AC_SRC_OVER;
	m_Blend.BlendFlags			= 0;
	m_Blend.BlendOp				= 0;
	m_Blend.SourceConstantAlpha = 255;

	SetGraphicsMode(m_AlphaDC, GM_ADVANCED);
	ReleaseDC(GameManager::GetInstance()->GethWnd(), hdc);
}

void Image::SetAlpha(BYTE Alpha)
{
	m_Blend.SourceConstantAlpha = Alpha;
}

BYTE Image::GetAlpha()
{
	return m_Blend.SourceConstantAlpha;
}

void Image::Render(HDC hdc)
{
	//BitBlt(hdc, 0, 0, m_Width, m_Height, m_MemDC, 0, 0, SRCCOPY );
}

void Image::Render(HDC hdc, float x, float y)
{
	//BitBlt( 
	//	hdc,				  // 목적지 DC
	//	x - ( m_Width  / 2 ), // 목적지DC에 뿌려질 Left
	//	y - ( m_Height / 2 ), // Top
	//	x + ( m_Width  / 2 ), // Right
	//	y + ( m_Height / 2 ), // Bottom
	//	m_MemDC,			  // 복사할 DC
	//	0,					  // 복사할 내용의 Left
	//	0,					  // 복사할 내용의 Top
	//	SRCCOPY);


	BitBlt( m_AlphaDC, 
			0,
			0,
			m_Width,
			m_Height,
			hdc,
			x - (m_Width / 2),
			y - (m_Height / 2),
			SRCCOPY);
		

	//// 특정색상의 픽셀을 제외하고 DC간 복사
	TransparentBlt
	(
		m_AlphaDC,				// 복사 받을 DC
		0,						// 찍을 x 위치
		0,						// 찍을 y 위치
		m_Width,				// 찍을 가로 크기
		m_Height,				// 찍을 세로 크기
		m_MemDC,				// 복사할 DC
		0,						// 찍을 원본의 위치 x
		0,						// 찍을 원본의 위치 x
		m_Width ,				// 찍을 원본의 크기 
		m_Height ,				// 찍을 원본의 크기
		RGB(255, 0, 255)		// 렌더링에서 제외할 픽셀 색상
	);

	//// TransparentBlt
	//// 1. #pragma comment (lib, "msimg32.lib") 추가후 사용
	//// 2. LTRB가 아님 , LTWH 이다.
	//// 3. 특정 색상의 픽셀을 제외하고 복사하는 ( Bitblt의 SRCCOPY ) 함수
	//// 4. 목적지DC에 찍을 크기를 원본크기보다 늘려주거나 줄여주면 이미지 scale 가능
	//// 5. 원본DC에 비트맵에 벗어나는 크기로 찍을경우 아예 안찍는다 ( 주의 )

	AlphaBlend(
		hdc,					// 복사 받을 DC
		x - (m_Width / 2),		// 찍을 x 위치
		y - (m_Height / 2),		// 찍을 y 위치
		m_Width,				// 찍을 가로 크기
		m_Height,				// 찍을 세로 크기
		m_AlphaDC,				// 복사할 DC
		0,						// 찍을 원본의 위치 x
		0,						// 찍을 원본의 위치 x
		m_Width,				// 찍을 원본의 크기 
		m_Height,				// 찍을 원본의 크기
		m_Blend);
}

XFORM GetResetTransform()
{
	XFORM Identy;
	Identy.eM11 = 1.f;
	Identy.eM12 = 0.f;
	Identy.eM21 = 0.f;
	Identy.eM22 = 1.f;
	Identy.eDx = 0.f;
	Identy.eDy = 0.f;
	return Identy;
}	

void Image::SetInverseDC(HDC hdc)
{
	m_InverseDC = hdc;
}
void Image::SetInverseBit(HBITMAP bit)
{
	m_InverseBit = bit;
}


void Image::Render(HDC hdc, XFORM Matrix)
{
	BitBlt(m_AlphaDC,
		0,	0, m_AlphaSize.GetX(), m_AlphaSize.GetY(),
		hdc, Matrix.eDx, Matrix.eDy, SRCCOPY);

	SetWorldTransform(m_MemDC, &Matrix);

	
	//// 특정색상의 픽셀을 제외하고 DC간 복사
	TransparentBlt
	(
		m_AlphaDC,				// 복사 받을 DC
		m_AlphaSize.GetX() / 2,		// 찍을 x 위치
		m_AlphaSize.GetY() / 2,		// 찍을 y 위치
		m_AlphaSize.GetX(),			// 찍을 가로 크기
		m_AlphaSize.GetY(),			// 찍을 세로 크기
		m_MemDC,				// 복사할 DC
		0,						// 찍을 원본의 위치 x
		0,						// 찍을 원본의 위치 x
		m_AlphaSize.GetX(),		// 찍을 원본의 크기 
		m_AlphaSize.GetY(),		// 찍을 원본의 크기
		RGB(255, 0, 255)		// 렌더링에서 제외할 픽셀 색상
	);
	SetWorldTransform(m_MemDC, &GetResetTransform() );

	AlphaBlend(
		hdc,					// 복사 받을 DC
		0,						// 찍을 x 위치
		0,						// 찍을 y 위치
		m_Width,				// 찍을 가로 크기
		m_Height,				// 찍을 세로 크기
		m_AlphaDC,				// 복사할 DC
		0,						// 찍을 원본의 위치 x
		0,						// 찍을 원본의 위치 x
		m_Width,				// 찍을 원본의 크기 
		m_Height,				// 찍을 원본의 크기
		m_Blend);
}

void Image::Render( HDC hdc, Transform* transform, 
					int SizeX, int SizeY, int OffSetX, int OffSetY )
{
	// 디폴트매개변수용 예외처리
	if (SizeX == 0) SizeX = m_Width;
	if (SizeY == 0) SizeY = m_Height;

	if (m_Blend.SourceConstantAlpha < 255)
	{
		BitBlt(m_AlphaDC, 0, 0, m_AlphaSize.GetX(), 
								m_AlphaSize.GetY(), hdc,
				transform->GetPosition().GetX() - m_AlphaSize.GetX() / 2,
				transform->GetPosition().GetY() - m_AlphaSize.GetY() / 2, SRCCOPY);

		transform->SetRotationTransform(m_AlphaDC, m_AlphaSize);
		// 특정색상의 픽셀을 제외하고 DC간 복사
		TransparentBlt
		(
			m_AlphaDC,										// 복사 받을 DC
			-SizeX  * transform->GetAnchorPoint().GetX(), 	// 찍을 x 위치
			-SizeY * transform->GetAnchorPoint().GetY(),	// 찍을 y 위치
			SizeX * transform->GetScale().GetX(),			// 찍을 가로 크기
			SizeY * transform->GetScale().GetY(),			// 찍을 세로 크기
			m_MemDC,										// 복사할 DC
			OffSetX,										// 찍을 원본의 위치 x
			OffSetY,										// 찍을 원본의 위치 x
			SizeX,											// 찍을 원본의 크기 
			SizeY,											// 찍을 원본의 크기
			RGB(255, 0, 255)								// 렌더링에서 제외할 픽셀 색상
		);
		transform->ResetWorldTransform(m_AlphaDC);

		AlphaBlend(
			hdc,														// 복사 받을 DC
			transform->GetPosition().GetX() - m_AlphaSize.GetX() / 2,	// 찍을 x 위치
			transform->GetPosition().GetY() - m_AlphaSize.GetY() / 2,	// 찍을 y 위치
			m_AlphaSize.GetX(),											// 찍을 가로 크기
			m_AlphaSize.GetY(),											// 찍을 세로 크기
			m_AlphaDC,													// 복사할 DC
			0,															// 찍을 원본의 위치 x
			0,															// 찍을 원본의 위치 x
			m_AlphaSize.GetX(),											// 찍을 원본의 크기 
			m_AlphaSize.GetY(),											// 찍을 원본의 크기
			m_Blend);
	}
	else
	{
		transform->SetWorldTransform(hdc, SizeX, SizeY );
		TransparentBlt
		(
			hdc,				// 복사 받을 DC
			0,					// 찍을 x 위치
			0,					// 찍을 y 위치
			SizeX * transform->GetScale().GetX(),// 찍을 가로 크기
			SizeY * transform->GetScale().GetY(), // 찍을 세로 크기
			m_MemDC,			// 복사할 DC
			OffSetX,			// 찍을 원본의 위치 x
			OffSetY,			// 찍을 원본의 위치 x
			SizeX,				// 찍을 원본의 크기 
			SizeY,				// 찍을 원본의 크기
			RGB(255, 0, 255)	// 렌더링에서 제외할 픽셀 색상
		);
	
		transform->ResetWorldTransform(hdc);
	}
}