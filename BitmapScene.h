#ifndef _BITMAPSCENE_H_
#define _BITMAPSCENE_H_

#include "Scene.h"

class Image;			// 전방선언 ( class Image가 프로젝트 어딘가에 있다는것만 알려준다 )

class BitmapScene : public Scene
{
public:
	BitmapScene();
	~BitmapScene();

public:
	virtual void	Init();
	virtual void	Update(float dt);
	virtual void	Render(HDC hdc);
	virtual void	Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

private:
	Image*			m_BackGround;
	Image*			m_Cursor;
	Image*			m_FadeInOut;
};


#endif