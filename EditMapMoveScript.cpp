#include "EditMapMoveScript.h"
#include "Input.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"

EditMapMoveScript::EditMapMoveScript()
{

}

EditMapMoveScript::~EditMapMoveScript()
{

}

void EditMapMoveScript::Init()
{

}

void EditMapMoveScript::Update(float dt)
{
	/*Renderer* renderer = GetComponent<Renderer>();
	Image* img = renderer->GetImage();*/
	float posX = m_Transform->GetPosition().GetX();
	float posY = m_Transform->GetPosition().GetY();
	float speed = 30.f;

	if (Input::GetKey(VK_LEFT))
	{
		//renderer->m_Transform
		posX += dt * speed;
	}
	else if (Input::GetKey(VK_RIGHT))
	{
		posX -= dt * speed;
	}

	m_Transform->SetPosition(Vector2(posX, posY));
}

void EditMapMoveScript::Release()
{

}