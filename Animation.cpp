#include "Animation.h"
#include "AnimationClip.h"
Animation::Animation() 
{
	m_NowAnimationClip = NULL;
}
Animation::~Animation()
{}
void Animation::Init()
{}
void Animation::Update(float dt) 
{
	if (m_NowAnimationClip != NULL)
		m_NowAnimationClip->Update(dt);
}
void Animation::Release()
{}
void Animation::Play()
{
	if(m_NowAnimationClip != NULL )
		m_NowAnimationClip->Play();
}
void Animation::Play(AnimationClip* Clip)
{
	m_NowAnimationClip = Clip;
	m_NowAnimationClip->Play();
}
void Animation::Stop()
{
	if (m_NowAnimationClip != NULL) m_NowAnimationClip->Stop();
}
bool Animation::IsPlaying() 
{
	if (m_NowAnimationClip == NULL)return false;
	return m_NowAnimationClip->IsPlay(); 
}
void Animation::SetAnimationClip(AnimationClip* Clip)
{
	m_NowAnimationClip = Clip;
}
AnimationClip* Animation::GetAnimationClip()
{
	return m_NowAnimationClip;
}