#include "Player.h"
#include "Input.h"
#include "GameObjectManager.h"
#include "SoundManager.h"

char* WalkSound[2]= {
	"Footstep_Dirt_00.mp3",
	"Footstep_Dirt_01.mp3",
	};

Player::Player()
{

}
Player::~Player()
{

}
void Player::Init()
{
	SOUND_MGR->SoundInit("Footstep_Dirt_00.mp3", false, false);
	SOUND_MGR->SoundInit("Footstep_Dirt_01.mp3", false, false);
	m_AniDir		= 0.f;		// 방향계산
	m_PlayerSpeed   = 200.f;	// 스피드
	m_Ani			= GetComponent < Animation>();
	m_Renderer		= GetComponent < Renderer>();
	// Idle 애니메이션 정보 설정
	m_Image[STATE_IDLE] = Image::GetImage("Idle.bmp");
	m_Clip[STATE_IDLE] = new ImageAnimationClip(m_GameObject);
	m_Clip[STATE_IDLE]->SetAnimationTime(1.f);			// 애니메이션 타임
	m_Clip[STATE_IDLE]->SetDirectionCnt(16);			// 몇방향 애니메이션?
	m_Clip[STATE_IDLE]->SetFrameCnt(16);				// 몇프레임?
	m_Clip[STATE_IDLE]->SetExitTime(false);				// 애니메이션 Stop시 애니메이션 끝난뒤 종료
	m_Clip[STATE_IDLE]->SetLoop(true);					// 반복하는 애니메이션?
	m_Clip[STATE_IDLE]->SetImage(m_Image[STATE_IDLE]);	// 애니메이션의 이미지 정보 설정
	// Run 애니메이션 정보 설정
	m_Image[STATE_RUN] = Image::GetImage("Run.bmp");
	m_Clip[STATE_RUN] = new ImageAnimationClip(m_GameObject);
	m_Clip[STATE_RUN]->SetAnimationTime(0.5f);
	m_Clip[STATE_RUN]->SetDirectionCnt(16);
	m_Clip[STATE_RUN]->SetFrameCnt(8);
	m_Clip[STATE_RUN]->SetExitTime(true);
	m_Clip[STATE_RUN]->SetLoop(true);
	m_Clip[STATE_RUN]->SetImage(m_Image[STATE_RUN]);
	// 최초 상태는 STATE_IDLE로 ..
	ChangeState(STATE_IDLE);

	// Map의 Transform 얻어오기
	m_MapScroll = OBJECT_MGR->FindObject("ObjectMap")->GetComponent<Transform>();
}
void Player::Update(float dt)
{
	if (Input::GetKeyDown(VK_RBUTTON))
	{
		
		Vector2 MapPos    = m_MapScroll->GetPosition();
		m_Dest			  = Input::GetMousePosition() - MapPos;		
		Vector2 PlayerPos = m_Transform->GetlocalPosition();
		Vector2	Dir		  = (m_Dest - PlayerPos).Normalize();
		m_AniDir		  = RADIAN_TO_ANGLE(atan2(Dir.GetY(), Dir.GetX())) + 270.f;

		if (m_AniDir >= 360.f)
			m_AniDir -= 360.f;

		ChangeState(STATE_RUN);
	}
	UpdateState(dt);
}

void Player::Release()
{
	for (int i = 0; i < STATE_END; i++)
	{
		delete m_Clip[i];
	}
}

void Player::ChangeState(STATE state)
{
	m_State = state;
	m_Ani->Play(m_Clip[m_State]);
	m_Renderer->SetImage(m_Image[m_State]);

}

void Player::UpdateState(float dt)
{
	m_Clip[m_State]->SetDirection(m_AniDir);
	switch (m_State)
	{
		case STATE_RUN:
		{
			static int Walk= 0;
			if (false == SOUND_MGR->IsPlaying( WalkSound[Walk]) )
			{
				Walk = !Walk;
				SOUND_MGR->SoundPlay(WalkSound[Walk]);
			}
			Vector2 Pos = m_Transform->GetlocalPosition();
			Vector2 Dir = (m_Dest - Pos).Normalize();
			Pos += Dir * dt * m_PlayerSpeed;
			// 목적지 도착여부 확인
			if (Pos.GetDistance(m_Dest) <= 5.f)
			{
				Pos = m_Dest;
				ChangeState(STATE_IDLE);
			}
			m_Transform->SetlocalPosition(Pos);
			break;
		}
	}
}