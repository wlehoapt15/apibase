#ifndef _INTROSCENE_H_
#define _INTROSCENE_H_
#include "Scene.h"
#include <Vfw.h>
class IntroScene : public Scene
{
public:
	IntroScene();
	~IntroScene();
public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
		WPARAM wParam, LPARAM lParam);

public:
	HWND	m_hMCI;
};
#endif
