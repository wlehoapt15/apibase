#include "ScrollScript.h"
#include "GameManager.h"
#include "GameObjectManager.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "LOG_MGR.h"

ScrollScript::ScrollScript()
{

}

ScrollScript::~ScrollScript()
{

}


void ScrollScript::Init()
{
	m_ScrollSpeed = 30.f;

	m_WinSizeX = GameManager::GetInstance()->GetRect().right;
	m_WinSizeY = GameManager::GetInstance()->GetRect().bottom;

	Renderer* renderer = GetComponent<Renderer>();
	m_MapSizeX = renderer->GetImage()->GetWidth();
	m_MapSizeY = renderer->GetImage()->GetHeight();
}

void ScrollScript::Update(float dt)
{
	float posX = m_Transform->GetPosition().GetX();
	float posY = m_Transform->GetPosition().GetY();

	posX -= dt * m_ScrollSpeed;
	//if (posX >= 0.f) posX = 0.f;

	if (posX + m_MapSizeX < 0.f) 
	{
		posX += m_MapSizeX;
		//OBJECT_MGR->FindObject("ScrollManager");
		INIT_LOG(LOG_CONSOLE);
		ADD_LOG("�̹��� �ٲ�!!\n");
	}

	m_Transform->SetPosition(Vector2(posX, posY));
}

void ScrollScript::Release()
{

}