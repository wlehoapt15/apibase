#include "GameManager.h"
#include "TimeManager.h"
#include "SceneManager.h"
#include "MainScene.h"		// 최초 시작할 씬
#include "HorizontalScene.h"
#include "Input.h"
#include "Image.h"
#include "LOG_MGR.h"
#include "SoundManager.h"
#include "GameObjectManager.h"
#include "IntroScene.h"
#include <iostream>

// static 변수는 소스코드 한곳에서 선언이 이루어저야 초기화 가능.
GameManager GameManager::m_Instance;

GameManager::GameManager()
{
	m_hWnd		 = NULL;
	m_hInstance  = NULL;
	m_BackDC	 = NULL;
	m_BackBitmap = NULL;
	// ZeroMemory , 해당하는 주소와 사이즈를 넘겨주면 모두 0으로 초기화
	ZeroMemory(&m_Rect, sizeof(RECT));
}

GameManager::~GameManager()
{
	// 일단 할일 없음..
}

int GameManager::Run(char* Title, int Width, int Height)
{
	// 윈도우 클래스 작성
	WNDCLASS WndClass		= { 0, };
	WndClass.hbrBackground  = (HBRUSH)GetStockObject(WHITE_BRUSH);
	WndClass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	WndClass.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpfnWndProc	= (WNDPROC)WndProc;
	WndClass.lpszClassName  = Title;
	WndClass.style			= CS_HREDRAW | CS_VREDRAW;
	// hInstance 얻어오기
	m_hInstance = WndClass.hInstance = GetModuleHandle(NULL);

	// 윈도우 클래스 등록
	RegisterClass(&WndClass);
	
	// 윈도우 작업영역 크기 맞추기
	RECT WindowRect = { 0, };
	AdjustWindowRect(&WindowRect, WS_OVERLAPPEDWINDOW, FALSE);

	m_Rect.right	= Width;
	m_Rect.bottom	= Height;

	m_hWnd = CreateWindow(
				Title,				// class 이름
				Title,				// 타이틀 이름
				WS_SYSMENU,			// 윈도우 스타일 ( 크기변경 X, 닫는 버튼만 )
				CW_USEDEFAULT,		// 시작 위치 X는 컴퓨터가 알아서
				CW_USEDEFAULT,		// 시작 위치 Y는 컴퓨터가 알아서
				Width  + ( WindowRect.right - WindowRect.left ), // 가로
				Height + ( WindowRect.bottom - WindowRect.top ), // 세로
				NULL,				// 부모윈도우의 핸들 없음
				NULL,				// 메뉴 없음
				m_hInstance,		// 인스턴스
				NULL				// 예약 (사용 X)
		);

	ShowWindow(m_hWnd, SW_SHOW);

	// 메시지 루프
	MSG Message = { 0, };

	// 게임환경에서는 GetMessage함수는 불편하다.
	// PeekMessage 함수를 이용한다. 
	// PeekMessage는 메시지가 없건 있건 바로 리턴하여 루프를 진행한다.
	
	// WM_QUIT 메시지가 발생하면 탈출
	while ( Message.message != WM_QUIT )	
	{
		TIME_MGR->Update();
		if (PeekMessage(&Message, NULL, NULL, NULL, PM_REMOVE))
		{
			// 메시지가 있을경우
			TranslateMessage(&Message);
			DispatchMessage(&Message);
		}
		Update( TIME_MGR->GetDeltaTime() );
		// Update 또는 Render를 진행
		InvalidateRect(m_hWnd, NULL, FALSE);
	}
	return Message.wParam;
}

LRESULT CALLBACK GameManager::WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_CREATE:
		{
			GetInstance()->m_hWnd = hWnd;
			GetInstance()->m_IsMoviePlay = false;

			INIT_LOG(LOG_CONSOLE);
			// BackBuffer만들기
			HDC hdc = GetDC(hWnd);
			GameManager::GetInstance()->m_BackDC = CreateCompatibleDC(hdc);

			GameManager::GetInstance()->m_BackBitmap =
				CreateCompatibleBitmap(hdc, 
				GameManager::GetInstance()->m_Rect.right,
				GameManager::GetInstance()->m_Rect.bottom); 

			SelectObject(GameManager::GetInstance()->m_BackDC, 
						 GameManager::GetInstance()->m_BackBitmap);

			Image::SetAlphaSize(Vector2( GameManager::GetInstance()->GetRect().right,
								GameManager::GetInstance()->GetRect().bottom ) );


			HDC		AlhpaDC = CreateCompatibleDC(hdc);
			HBITMAP AlphaBit = CreateCompatibleBitmap(
				hdc, GameManager::GetInstance()->GetRect().right,
					 GameManager::GetInstance()->GetRect().bottom );

			HDC InverseDC = CreateCompatibleDC(hdc);
			HBITMAP InverseBit = CreateCompatibleBitmap(hdc,
				GameManager::GetInstance()->GetRect().right,
				GameManager::GetInstance()->GetRect().bottom
			);
			// 알파블랜드할 DC
			Image::SetAlphaDC(AlhpaDC);
			// 알파블랜드할 비트맵
			Image::SetAlphaBitmap(AlphaBit);
			SelectObject(AlhpaDC, AlphaBit);
			// 알파블랜드할 DC
			Image::SetInverseDC(InverseDC);
			Image::SetInverseBit(InverseBit);
			SelectObject(InverseDC, InverseBit);
			// 알파블랜드할 비트맵

			// 사운드 매니저 초기화
			SOUND_MGR->Init();
			// 그래픽모드 바꾸기
			SetGraphicsMode(GameManager::GetInstance()->m_BackDC, GM_ADVANCED);

			// 최초의 씬변경
			SCENE_MGR->ChangeScene<IntroScene>();
			//SCENE_MGR->ChangeScene<HorizontalScene>();
			ReleaseDC(hWnd, hdc);

			ADD_LOG("윈도우초기화 완료 %d", 1234);
			break;
		}

		case WM_PAINT:
		{
			// Render 처리
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			FillRect(GameManager::GetInstance()->m_BackDC,
					 &GameManager::GetInstance()->m_Rect,
					 (HBRUSH)GetStockObject(WHITE_BRUSH));

			////////////////////////////////////////
			// BackDC에 찍기
			GameManager::GetInstance()->Render
			(
				GameManager::GetInstance()->m_BackDC
			);
			if (GetInstance()->m_IsMoviePlay == false)
			{
				///////////////////////////////////////
				// 최종 화면DC에 찍기
				BitBlt(hdc, 0, 0,
					GameManager::GetInstance()->m_Rect.right,
					GameManager::GetInstance()->m_Rect.bottom,
					GameManager::GetInstance()->m_BackDC,
					0, 0, SRCCOPY);
			}
			///////////////////////////////////////
			EndPaint(hWnd, &ps);
			break;
		}

		case WM_SETFOCUS:		// 현재 윈도우창이 포커스가 될떄
		{
			Input::SetFocusWindow( true );
			break;
		}

		case WM_KILLFOCUS:		// 현재 윈도우창이 포커스가 해제될때
		{
			Input::SetFocusWindow( false );
			break;
		}

		case WM_DESTROY:
		{
			PostQuitMessage(0);
			break;
		}
	}
	return SCENE_MGR->SceneProc(hWnd, iMessage, wParam, lParam);
}

void GameManager::Update(float dt)
{
	// 전역적으로 해야하는 갱신
	OBJECT_MGR->Update(dt);
	SCENE_MGR->Update(dt);
	SOUND_MGR->Update();
	
}

void GameManager::Render(HDC hdc)
{

		OBJECT_MGR->Render(hdc);
		SCENE_MGR->Render(hdc);

		// 전역적으로 해야하는 렌더
		char str[255];
		sprintf(str, "World Time : %.0f", TIME_MGR->GetWorldTime());
		TextOut(hdc, 0, 0, str, strlen(str));
		sprintf(str, "FPS : %.0f", TIME_MGR->GetFPS());
		TextOut(hdc, 0, 15, str, strlen(str));
		sprintf(str, "DeltaTime : %f", TIME_MGR->GetDeltaTime());
		TextOut(hdc, 0, 30, str, strlen(str));
	
}