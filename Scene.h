#ifndef _SCENE_H_
#define _SCENE_H_

///////////////////////////////////////////////////////
// 씬 클래스
// 작성자	: 
// 작성날짜	: 2017.05.09
// 용도		: 게임의 단위인 씬의 추상클래스	  
//////////////////////////////////////////////////////

#include <Windows.h>

class Scene
{
public:
	Scene();
	virtual ~Scene();

	// 순수 가상함수들
public:
	virtual void Init()				= 0;
	virtual void Update(float dt)	= 0;
	virtual void Render(HDC hdc)	= 0;
	virtual void Release()			= 0;
	virtual LRESULT SceneProc( HWND hWnd, UINT iMessage, 
							   WPARAM wParam, LPARAM lParam) = 0;
private:
	float m_LoadingProgress;
};


#endif