#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_

#include "SoundChannelInfo.h"
#include <map>
#include <string>
#pragma comment( lib, "FMOD/fmodex_vc.lib")

#define SOUND_MGR SoundManager::GetInstance()

class SoundManager
{
public:
	static SoundManager* GetInstance()
	{
		if (m_Instance == NULL) m_Instance = new SoundManager;
		return m_Instance;
	}
private:
	// 싱글턴 패턴 형태로 클래스 만들기
	static SoundManager* m_Instance;
	SoundManager();
	SoundManager(const SoundManager& val);					// 복사생성 방지
	SoundManager& operator = (const SoundManager& val);		// 대입방지
	~SoundManager();

public: // 인터페이스
	bool	Init(int ChannelCnt = 32);							 // 시스템 초기화
	void	SoundInit(char* SoundName, bool IsBGM, bool IsLoop); // 사운드 초기화
	int		SoundPlay(char* SoundName, float Volume = 1.0f);	 // 사운드 재생
	void	SoundStop(char* SoundName);							// 사운드 정지
	void	SoundStop(int ChannelIndex);						// 사운드 정지
	void	SetSoundVolume(char* SoundName, float Volume);		 // 볼륨 변경 ( 사운드로 찾아서 )
	void	SetSoundVolume(int ChannelIndex, float Volume);		 // 특정채널의 볼륨변경
	void	SoundPause(char* SoundName);		// 사운드 일시정지
	void	SoundPause(int ChannelIndex);		// 특정채널의 사운드 일시정지 
	void	SoundResume(char* SoundName);		// 일시정지 사운드 재개
	void	SoundResume(int ChannelIndex);		// 특정채널의 일시정지 사운드 재개
	bool	IsPlaying(char* SoundName);			// 사운드 재생확인
	bool	IsPlaying(int ChannelIndex);		// 특정채널의 사운드 재생 확인
	bool	IsPause(char* SoundName);			// 사운드 일시정지중인지 확인
	bool	IsPause(int ChannelIndex);			// 특정채널의 사운드가 일시정지중인지..
	void	Update();							// FMOD 시스템 업데이트
	void	Release();							// FMOD 시스템 정리

private: // 맴버변수
	FMOD::System*							m_System;		// FMOD 시스템
	SoundChannelInfo*						m_ChannelList;	// 채널 리스트
	std::map< std::string, FMOD::Sound* >	m_SoundList;	// 사운드 리스트
	int										m_ChannelCnt;	// 채널 갯수
};


#endif