#include "HorizontalScene.h"
#include "SoundManager.h"
#include "GameObjectManager.h"
#include "Bitmap.h"
#include "ScrollMapObject.h"
#include "PigObject.h"

HorizontalScene::HorizontalScene() 
{

}

HorizontalScene::~HorizontalScene()
{
		
}

void HorizontalScene::Init() 
{
	SOUND_MGR->SoundInit("BGM.mp3", true, true);
	GameObject* map = new ScrollMapObject;
	OBJECT_MGR->AddObject(map);
	
	GameObject* pig = new PigObject();
	OBJECT_MGR->AddObject(pig, Vector2(400.f, 300.f));
	//Component* scrolling_manager = OBJECT_MGR->FindObject("object name")->GetComponent<customscript>();
/*
	GameObject* Player = new ObjectPlayer;
	Player->GetComponent<Transform>()->SetParent(Map->GetComponent<Transform>());
	OBJECT_MGR->AddObject(Player, Vector2(400.f, 300.f));
	SOUND_MGR->SoundPlay("BGM.mp3", 0.1);
	GameObject* obj = new Amazon();
	OBJECT_MGR->AddObject(obj, Vector2(300.f, 300.f));*/
}

void HorizontalScene::Update(float dt) {
	OBJECT_MGR->Update(dt);
}

void HorizontalScene::Render(HDC hdc) {

}

void HorizontalScene::Release() {

}

LRESULT HorizontalScene::SceneProc(HWND hWnd, UINT iMessage,
	WPARAM wParam, LPARAM lParam) {

	return DefWindowProc(hWnd, iMessage, wParam, lParam);
}
