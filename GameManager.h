#ifndef _GAMEMANAGER_H_
#define _GAMEMANAGER_H_

///////////////////////////////////////////////////////
// 게임 매니저 클래스 ( 관리자 )
// 작성자	: 
// 작성날짜	: 2017.05.28
// 용도		: 윈도우 생성, 게임의 전반적인 로직 관리
//			  메인 프레임( 뼈대 ) 
//////////////////////////////////////////////////////

#include <Windows.h>

class GameManager
{
	// 관리자 클래스의 경우 객체가 프로그램(프로세스)내에
	// 유일하게 하나만 존재해야 한다.
	// 디자인패턴 -> 클래스를 어떻게 설계하느냐에 대한 방법론
	// 싱글턴패턴 -> 객체가 유일하게 하나임이 보장되는 클래스 설계방식
	// 핵심. private 생성자, static 객체
private:	// 생성자 소멸자
	GameManager();
	GameManager(const GameManager& Val);			 // 복사생성 금지 ( 정의 X )
	GameManager& operator = (const GameManager& Val);// 대입도 금지 ( 정의 X )
	~GameManager();

private:
	static GameManager m_Instance;	// 객체를 static으로 선언
	// 윈도우 프로시저
	static LRESULT CALLBACK 
	WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);
public:
	static GameManager* GetInstance() { return &m_Instance; }

public: // 사용자 함수 ( 윈도우 제목, 창의 가로, 세로 크기 )
	int		Run( char* Title, int x, int y );

public:		// 접근자 함수
	inline HDC GetBackDC()			{ return m_BackDC;	  }
	inline RECT GetRect()			{
		return m_Rect;	 
	}
	inline HWND GethWnd()			{ 
		return m_hWnd;	  
	}
	inline HINSTANCE GethInstance() { return m_hInstance; }
	// 동영상 재생중인지 확인
	void SetMoviePlay(bool Play) { m_IsMoviePlay = Play; }
private:
	void Update( float dt );
	void Render(HDC hdc);

private:	// 맴버 변수
	HWND		m_hWnd;			// 윈도우	 핸들
	HINSTANCE	m_hInstance;	// 프로세스의 핸들
	// 백버퍼용 변수 ( 더블 버퍼링 , 깜빡임 방지 ) 
	HDC			m_BackDC;		// 백버퍼DC
	HBITMAP		m_BackBitmap;	// 백버퍼Bitmap
	RECT		m_Rect;			// 윈도우 창크기
	bool		m_IsMoviePlay;	// 동영상 재생중인지
};
#endif