	#include "TimeManager.h"
#pragma comment ( lib, "winmm.lib")

// 표준 시간측정함수
// 1. time()
// 2. clock()
// 3. c++ -> #include <chrono> 

// WINAPI 시간측정함수				공통점				차이점 ( 타임 해상도 )
// 1. GetTickCount()				부팅완료후			15/1000
// 2. timeGetTime()					지난시간을			2~1/1000
// 3. QueryPerformanceCounter()		1/1000초 단위반환	하드웨어가 지원하는 정밀도

TimeManager* TimeManager::m_Instance = NULL;

TimeManager::TimeManager()
{
	// 하드웨어 타이머 지원여부 확인 ( 반환값 )
	// 하드웨어 정밀도 확인 ( 매개변수 LARGE_INTAGER* )
	m_IsHardwareAble = QueryPerformanceFrequency(&m_Frequency);
	m_DeltaTime = m_WorldTime = 0.f;

	if (m_IsHardwareAble == TRUE)
	{
		QueryPerformanceCounter(&m_CheckTime);
		m_BeginTime = m_CheckTime.QuadPart;
	}
	else
	{
		m_BeginTime = timeGetTime();
	}
}

TimeManager::~TimeManager()
{

}

void	TimeManager::Update()
{
	// 시간 측정 시작.
	// 하드웨어 지원여부 확인
	if (m_IsHardwareAble == TRUE)
	{
		QueryPerformanceCounter(&m_CheckTime);
		m_EndTime = m_CheckTime.QuadPart;
	}
	else
	{
		m_EndTime = timeGetTime();
	}

	// 델타타임 계산
	m_DeltaTime = (float)(m_EndTime - m_BeginTime)
				  / ( (m_IsHardwareAble == FALSE) ? 1000 : m_Frequency.QuadPart);

	// 월드타임 계산
	m_WorldTime += m_DeltaTime;

	// 시작시간을 현재 시간으로
	m_BeginTime = m_EndTime;
}

float	TimeManager::GetFPS()
{
	static int		Frames	   = 0;
	static float	FPS		   = 0.f;
	static float	UpdateTime = 0.f;

	Frames++;
	UpdateTime += m_DeltaTime;

	if (UpdateTime >= 0.5f )
	{
		FPS			= Frames / UpdateTime;
		UpdateTime  = 0.f;
		Frames		= 0;
	}

	return FPS;
}