#include "GameObject.h"
#include "Transform.h"
#include "Renderer.h"
GameObject::GameObject()
{
	AddComponent<Transform>();
}

GameObject::~GameObject()
{

}

void GameObject::Init()
{

}

void GameObject::Update(float dt)
{
	if (m_IsActive == false) return;
	for (int i = 0; i < m_ComponentList.size(); i++)
	{
		if (m_ComponentList[i]->GetEnable() == true)
		{
			m_ComponentList[i]->Update(dt);
		}
	}
}

void GameObject::Render(HDC hdc)
{
	if (m_IsActive == false) return;
	Renderer* render = GetComponent<Renderer>();
	if (render != NULL && render->GetEnable() ) render->Render(hdc);
}

void GameObject::Release()
{
	for (int i = 0; i < m_ComponentList.size(); i++)
	{
		m_ComponentList[i]->Release();
		delete m_ComponentList[i];
	}
	m_ComponentList.clear();
}