#include "UiObject.h"
#include "Image.h"
#include "Renderer.h"

UiObject::UiObject()
{
	m_NormalImage	= NULL;
	m_OverImage		= NULL;
}

UiObject::~UiObject()
{

}

void UiObject::Init()
{
	m_NormalImage = Image::GetImage("start_normal.bmp");
	m_OverImage = Image::GetImage("start_over.bmp");

	m_Renderer = AddComponent<Renderer>();
	m_Renderer->SetImage(m_OverImage);
	
	m_IsPointerEnter	= false;
	m_IsPointerDown		= false;
}

void UiObject::Release()
{

}

void UiObject::SetNormalImage()
{
	m_Renderer->SetImage(m_NormalImage);
}

void UiObject::SetOverImage()
{
	m_Renderer->SetImage(m_OverImage);
}

void UiObject::OnPointerEnter(float dt)
{
	m_Renderer->SetImage(m_NormalImage);
}

void UiObject::OnPointerDown(float dt)
{

}

void UiObject::OnPointerUp(float dt)
{

}

void UiObject::OnPointerStay(float dt)
{

}

void UiObject::OnPointerExit(float dt)
{
	m_Renderer->SetImage(m_OverImage);
}

void UiObject::SetNormalImage(Image* img)
{
	m_NormalImage = img;
	m_Renderer->SetImage(m_NormalImage);
}

void UiObject::SetOverImage(Image* img)
{
	m_OverImage = img;
	m_Renderer->SetImage(m_OverImage);
}		