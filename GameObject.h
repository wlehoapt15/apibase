#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_
#include "Object.h"
#include <Windows.h>
#include <vector>
///////////////////////////////////////////////////////
// 게임 오브젝트 클래스
// 작성자	: 
// 작성날짜	: 2017.05.18
// 용도		: Scene에서 월드상에 배치되는 게임의 기본 오브젝트 클래스
//			 		  
//////////////////////////////////////////////////////

class Component;
class Transform;

class GameObject : public Object
{
public:
	GameObject();
	~GameObject();

public:		
	virtual void Init();
	void	Update( float dt );
	void	Render( HDC hdc);
	virtual void Release();

public:
	virtual std::string ToString()			{ return std::string("GameObject ") + m_Name; }
	void				SetName(char* Name) { m_Name = std::string(Name);   }
	std::string			GetName()			{ return m_Name;				}
	template <typename COMPONENT>
	COMPONENT*			GetComponent()
	{
		for (int i = 0; i < m_ComponentList.size(); i++)
		{
			if (typeid(COMPONENT) == typeid(*m_ComponentList[i]) )
			{
				return static_cast<COMPONENT*>(m_ComponentList[i]);
			}
		}
		return NULL;
	}
	template <typename COMPONENT>
	COMPONENT*			AddComponent()
	{
		for (int i = 0; i < m_ComponentList.size(); i++)
		{
			if (typeid(COMPONENT) == typeid(*m_ComponentList[i]))
			{
				return NULL;
			}
		}

		Component* NewComponent = dynamic_cast<COMPONENT*>( new COMPONENT );
		if (NewComponent == NULL) {	return NULL; }
		NewComponent->SetGameObject(this);
		if (m_ComponentList.empty() )
		{
			m_Transform = (Transform*)( NewComponent );
		}
		NewComponent->SetTransform(m_Transform);
		NewComponent->Init();
		m_ComponentList.push_back(NewComponent);
		return static_cast<COMPONENT*>(NewComponent);
	}

	bool GetActive()			{ return m_IsActive;   }
	void SetActive(bool Active) { m_IsActive = Active; }

protected:
	std::string					m_Name;
	std::vector<Component*>		m_ComponentList;
	Transform*					m_Transform;
	bool						m_IsActive;
};

#endif