#include "Object.h"

Object::Object()
{

}
Object::~Object()
{

}

// 보통의 경우 Debug등에서 찍을 Object의 이름이나 타입등을 반환
std::string Object::ToString()
{
	return typeid(*this).name();
}

int	 Object::GetHash() const
{
	// 고유ID로 자기가 할당된 메모리 주소를 반환
	return reinterpret_cast<int>(this);
}

bool Object::operator == (const Object& Val) const
{
	return GetHash() == Val.GetHash();
}
bool Object::operator != (const Object& Val) const
{
	return GetHash() != Val.GetHash();
}