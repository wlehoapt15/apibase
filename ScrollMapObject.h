#ifndef  _SCROLLMAPOBJ_H_
#define _SCRROLLMAPOBJ_H_

#include "GameObject.h"

class ScrollMapObject : public GameObject
{
public:
	ScrollMapObject();
	~ScrollMapObject();

public:
	virtual void Init();
	virtual void Release();

public:
	int roundCnt;
	void AddCount();

private :
	std::vector<GameObject*> m_MapList;
	//Vector2 m_BackgroundObjPos;
};

#endif // ! _SCROLLMAPOBJ_H_
