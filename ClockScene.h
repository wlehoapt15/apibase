#ifndef _CLOCKSCENE_H_
#define _CLOCKSCENE_H_

#include "Scene.h"

class ClockScene : public Scene
{
public:
	ClockScene();
	~ClockScene();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
							  WPARAM wParam, LPARAM lParam);
private:
	SYSTEMTIME		m_NowTime;
};


#endif