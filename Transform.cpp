#include "Transform.h"
#include <windows.h>
XFORM Transform::m_Identity = { 1.0f, 0.0f ,0.0f ,1.0f ,0.0f ,0.0f };

Transform::Transform()
{
	m_Position		 = Vector2( 0.f, 0.f );
	m_localPosition  = Vector2( 0.f, 0.f );
	m_Scale			 = Vector2( 1.f, 1.f );
	m_localScale	 = Vector2( 1.f, 1.f );
	m_AnchorPoint	 = Vector2(0.f, 0.f);
	m_Rotation		 = 0.f;
	m_localRotation  = 0.f;
	m_WorldMatrix	 = m_Identity;
	m_Parent		 = NULL;
}

Transform::~Transform()
{

}

void		Transform::Init()
{

}

void		Transform::Release()
{

}

void		Transform::Update(float dt)
{
	m_WorldMatrix.eM11 = cos(m_Rotation);
	m_WorldMatrix.eM12 = sin(m_Rotation);
	m_WorldMatrix.eM21 = -sin(m_Rotation);
	m_WorldMatrix.eM22 = cos(m_Rotation);
	m_WorldMatrix.eDx  = m_Position.GetX();
	m_WorldMatrix.eDy  = m_Position.GetY();
	SetlocalPosition(m_localPosition);
	SetlocalScale(m_localScale);
	SetlocalRotation(m_localRotation);
}

Vector2		Transform::GetPosition()
{
	return m_Position;
}


Vector2		Transform::GetlocalPosition()
{
	return m_localPosition;
}

void		Transform::SetPosition(Vector2& Pos)
{
	if (m_Parent == NULL)
	{
		m_Position = m_localPosition = Pos;
	}
	else
	{
		m_Position		= Pos;
		m_localPosition = Pos - m_Parent->m_localPosition;
	}
}

void		Transform::SetlocalPosition(Vector2& Pos)
{
	if (m_Parent == NULL)
	{
		m_Position		= m_localPosition = Pos;
	}
	else
	{
		m_Position		= m_Parent->m_localPosition + Pos;
		m_localPosition = Pos;
	}
}
			
Vector2		Transform::GetScale()
{
	return m_Scale;
}

Vector2		Transform::GetlocalScale()
{
	return m_localScale;
}

void		Transform::SetScale(Vector2& Scale)
{
	if (m_Parent == NULL)
	{
		m_Scale = m_localScale = Scale;
	}
	else
	{
		m_Scale = Scale;
		m_localScale = Vector2( Scale.GetX() / m_Parent->m_localScale.GetX(),
								Scale.GetY() / m_Parent->m_localScale.GetY());
	}
}

void		Transform::SetlocalScale(Vector2& Scale)
{
	if (m_Parent == NULL)
	{
		m_Scale = m_localScale = Scale;
	}
	else
	{
		m_Scale = Vector2( m_Parent->m_localScale.GetX() * Scale.GetX(),
						   m_Parent->m_localScale .GetY() * Scale.GetY());
		m_localScale = Scale;
	}
}
			
float		Transform::GetRotation()
{
	return m_Rotation;
}
float		Transform::GetlocalRotation()
{
	return m_localRotation;
}

void		Transform::SetRotation(float Radian)
{
	if (m_Parent == NULL)
	{
		m_Rotation = m_localRotation = Radian;
	}
	else
	{
		m_Rotation		= Radian;
		m_localRotation = Radian - m_Parent->m_localRotation;
	}
}

void		Transform::SetlocalRotation(float Radian)
{
	if (m_Parent == NULL)
	{
		m_Rotation = m_localRotation = Radian;
	}
	else
	{
		m_Rotation		= m_Parent->m_localRotation + Radian;
		m_localRotation = Radian;
	}
}

void		Transform::SetParent(Transform* Parent)
{
	m_Parent = Parent;
}

Transform*	Transform::GetParent()
{
	return m_Parent;
}

void		Transform::SetWorldTransform(HDC hdc, int SizeX, int SizeY )
{
	m_WorldMatrix.eDx = m_Position.GetX() - (SizeX * m_AnchorPoint.GetX());
	m_WorldMatrix.eDy = m_Position.GetY() - (SizeY * m_AnchorPoint.GetY());
	::SetWorldTransform(hdc, &m_WorldMatrix);
}

void		Transform::SetTranslateTransform(HDC hdc, float OffSet)
{
	XFORM TranslateMatrix = m_Identity;
	TranslateMatrix.eDx = -OffSet / 2.f;
	TranslateMatrix.eDy = -OffSet / 2.f;
	::SetWorldTransform(hdc, &TranslateMatrix);
}

void		Transform::SetRotationTransform(HDC hdc, Vector2 Size )
{
	XFORM RotMatrix = m_WorldMatrix;
	RotMatrix.eDx = Size.GetX() / 2.f;
	RotMatrix.eDy = Size.GetY() / 2.f;
	::SetWorldTransform(hdc, &RotMatrix);
}

void		Transform::ResetWorldTransform(HDC hdc)
{
	::SetWorldTransform(hdc, &m_Identity);
}

Vector2	 Transform::GetAnchorPoint()
{
	return m_AnchorPoint;
}

void Transform::SetAnchorPoint(Vector2 Anchor)
{
	m_AnchorPoint = Anchor;
}