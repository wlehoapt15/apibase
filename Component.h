#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include "GameObject.h"

class Transform;

class Component : public Object
{
	friend class GameObject;
public:
	Component();
	~Component();

public:
	virtual void Init()				= 0;
	virtual void Update( float dt )	= 0;
	virtual void Release()			= 0;

public:
	template <typename COMPONENT> 
	COMPONENT* AddComponent()
	{
		if (m_GameObject == NULL) return NULL;
		return m_GameObject->AddComponent<COMPONENT>();
	}

	template <typename COMPONENT>
	COMPONENT* GetComponent()
	{
		if (m_GameObject == NULL) return NULL;
		return m_GameObject->GetComponent<COMPONENT>();
	}
	void SetEnable(bool Enable) { m_Enable = Enable; }
	bool GetEnable() { return m_Enable; }

protected:
	// 컴포넌트가 속해있는 게임오브젝트 설정하는 함수
	void SetGameObject(GameObject* Obj)		{ m_GameObject = Obj;		}
	void SetTransform(Transform* transform) { m_Transform = transform;  }
	// 컴포넌트 끄기
protected:
	bool		m_Enable;
	GameObject*	m_GameObject;
	Transform*	m_Transform;
};

#endif