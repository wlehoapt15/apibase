#ifndef _IMAGE_H_
#define _IMAGE_H_

///////////////////////////////////////////////////////
// 이미지 클래스
// 작성자	: 
// 작성날짜	: 2017.05.12
// 용도		: 이미지 관리, 이미지 출력
//////////////////////////////////////////////////////
#include <Windows.h>
#include "Vector2.h"
class Transform;

class Image
{
public:	// 이미지 관리용 맴버
	static Image* GetImage(char* FileName);		// 파일 이름으로 이미지 로딩 (없으면 생성)
	static void	  RemoveImage(char* FileName);  // 현재 불러온 이미지 제거
	static void	  Release();					// 현재 불러온 이미지 모두 제거

public:
	Image();
	Image(char* FileName);
	~Image();

public:
	void Init(char* FileName);					// 이미지 초기화
	void Render(HDC hdc);						// 이미지 출력 ( 좌표 0, 0 )
	void Render(HDC hdc, float x, float y);		// 이미지 출력 ( 좌표 x, y )
	void Render(HDC hdc, XFORM Matrix);			// 이미지 출력 ( 변환행렬 이용 ) 
	void Render(HDC hdc, Transform* transform, int SizeX = 0, int SizeY = 0,
											   int OffSetX = 0, int OffSetY = 0 );// 이미지 출력 ( Trnasform 컴포넌트 ) 
	int	 GetWidth()		{ return m_Width;  }
	int  GetHeight()	{ return m_Height; }

	void SetAlpha(BYTE Alpha);
	BYTE GetAlpha ();

	static void SetAlphaSize(Vector2 Size);
	static void SetAlphaDC(HDC hdc );
	static void SetAlphaBitmap(HBITMAP bit);
	static void SetInverseDC( HDC hdc );
	static void SetInverseBit( HBITMAP bit );
private:
	int					 m_Width;		// 이미지 가로 크기
	int					 m_Height;		// 이미지 세로 크기

	HBITMAP				 m_hBit;		// 비트맵핸들
	HDC					 m_MemDC;		// 비트맵을 선택할 DC
	static HBITMAP		 m_AlphaBit;	// 알파블랜드할 비트맵
	static HDC			 m_AlphaDC;		// 알파블랜드할 DC
	static HBITMAP		 m_InverseBit;
	static HDC			 m_InverseDC;
	static Vector2		 m_AlphaSize;	// 이미지 가로세로 크기중 큰값
	BLENDFUNCTION		 m_Blend;		// 알파블랜드용도의 구조체
};


#endif