#ifndef _INPUT_H_
#define _INPUT_H_

#include <Windows.h>
#include "Vector2.h"

namespace Input
{
	// 윈도우 포커스 설정 함수
	void SetFocusWindow(bool Focus);

	// 키보드( 마우스 버튼 ) 눌리는중인지 확인
	bool GetKey(int VK_KEY);

	// 키보드( 마우스 버튼 ) 한번 눌렸는지 확인 
	bool GetKeyDown(int VK_KEY);

	// 키보드( 마우스 버튼 ) 땔때 확인 
	bool GetKeyUp(int VK_KEY);

	// 마우스 좌표 얻어오기
	Vector2 GetMousePosition();
}

#endif 