#ifndef _OBJECT_H_
#define _OBJECT_H_


///////////////////////////////////////////////////////
// 오브젝트 클래스
// 작성자	: 
// 작성날짜	: 2017.05.28
// 용도		: 게임내 모든 오브젝트들의 최상위 클래스
//			 		  
//////////////////////////////////////////////////////

#include <string>

class Object
{
public:
	Object();
	~Object();

public:
	virtual std::string ToString();				// 보통의 경우 Debug등에서 찍을 Object의 이름이나 타입등을 반환
	int		GetHash() const;					// 현재 오브젝트들의 고유 ID등을 반환
	bool operator == (const Object& Val) const;	// 오브젝트 비교 확인
	bool operator != (const Object& Val) const;
};


#endif