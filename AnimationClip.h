#ifndef _ANIMATIONCLIP_H_
#define _ANIMATIONCLIP_H_

class GameObject;

class AnimationClip
{
public:
	AnimationClip(GameObject* Obj);
	virtual ~AnimationClip();

public:
	virtual void Init();
	virtual void Update(float dt);

public:
	void	SetAnimationTime(float Time) { m_AnimationTime = Time; }
	float	GetProgress()	  {	return m_Progress; }
	int		GetFrameCnt()	  { return m_FrameCnt; }
	int		GetDirectionCnt() { return m_DirectionCnt; }
	bool	IsLoop()		  { return m_IsLoop; }
	bool	IsPlay()		  { return m_IsPlay; }
	void	SetLoop(bool loop) { m_IsLoop = loop;  }
	void	SetExitTime(bool IsExit) { m_HasExitTime = IsExit;  }
	void	SetDirection(float Angle)
	{
		if (m_DirectionCnt > 0)
		{
			m_NowDirection = (Angle / 360.f) * m_DirectionCnt;
		}
	}

	virtual void SetFrameCnt(int Cnt);
	virtual void SetDirectionCnt(int Cnt);
	virtual void Play();
	virtual void Stop();

protected:
	float	m_Progress;			// 0.0 ~1.0 사이의 숫자, 애니메이션 진행률
	float	m_AnimationTime;	// 애니메이션 시간
	int		m_FrameCnt;			// 애니메이션 갯수 ( 가로줄의 이미지 )
	int		m_DirectionCnt;		// 애니메이션 방향의 갯수 ( 세로줄의 이미지 )
	
	int		m_NowDirection;		// 현재 애니메이션의 방향
	int		m_LoopCnt;			// 애니메이션 Play후 몇번 반복중인지

	bool	m_IsPlay;			// 현재 재생중인지
	bool	m_IsLoop;			// 반복재생이 가능한 애니메이션인지
	bool	m_HasExitTime;		// 애니메이션 Stop시에 프레임이 
								// 완료될때까지 재생할것인지
	GameObject* m_GameObject;	// 현재 애니메이션 클립을 소유하고있는 오브젝트
};

#endif