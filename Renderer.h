#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "Component.h"

class Image ;
class Renderer : public Component
{
public:
	Renderer();
	~Renderer();

public:
	virtual void Init();
	virtual void Update(float dt);
	void		 Render(HDC hdc);
	void		 Release();

	void		 SetImage(Image* image);
	Image*		 GetImage();
	void		 SetAlpha(float Alpha);
	float 		 GetAlpha();

	void SetOffSetX(int offset) { m_OffSetX = offset; }
	void SetOffSetY(int offset) { m_OffSetY = offset; }
	void SetSizeX(int size) { m_SizeX = size;  }
	void SetSizeY(int size) { m_SizeY = size;  }

private:
	Image*		 m_Image;
	float		 m_Alpha;

	int			 m_OffSetX;
	int			 m_OffSetY;
	int			 m_SizeX;
	int			 m_SizeY;
};

#endif
