#include "Vector2.h"
#include <math.h>

// 생성자 소멸자
Vector2::Vector2()
{
	m_x = m_y = 0.f;
}

Vector2::Vector2(float x, float y)
{
	m_x = x;
	m_y = y;
}

Vector2::Vector2(const Vector2& Val)
{
	m_x = Val.m_x;
	m_y = Val.m_y;
}

// 맴버 연산자
Vector2  Vector2::operator + (const Vector2& Val)
{
	return Vector2(m_x + Val.m_x, m_y + Val.m_y);
}

Vector2  Vector2::operator - (const Vector2& Val)
{
	return Vector2(m_x - Val.m_x, m_y - Val.m_y);
}

Vector2& Vector2::operator += (const Vector2& Val)
{
	return *this = *this + Val;
}

Vector2& Vector2::operator -= (const Vector2& Val)
{
	return *this = *this - Val;
}

Vector2  Vector2::operator *(float Val)
{
	return Vector2(m_x * Val, m_y * Val);
}

Vector2& Vector2::operator *=(float Val)
{
	return *this = *this * Val;
}

Vector2  Vector2::operator /(float Val)
{
	if (Val == 0.f) return Vector2(0.f, 0.f);
	return Vector2(m_x / Val, m_y / Val);
}

Vector2& Vector2::operator /=(float Val)
{
	return *this = *this / Val;
}

// 맴버 함수
float	Vector2::GetDistance() 
{
	return sqrt(m_x * m_x + m_y * m_y );
}

float	Vector2::GetDistance(const Vector2& Val) 
{
	return (*this - Val).GetDistance();
}

Vector2	Vector2::Normalize()
{
	return *this / GetDistance(); 
}

float	Vector2::Dot(const Vector2 Val) 
{
	// 내적공식 
	// 벡터A ( x1,  y1 ), 벡터B( x2, y2 )가 있을때 
	// 기하학적 의미 : 벡터A길이 * 벡터B길이 * cos( 두벡터가 이루는 각도 )
	// 공식			: ( x1 * x2 ) + ( y1 * y2 )
	return (m_x * Val.m_x) + (m_y * Val.m_y);
}

