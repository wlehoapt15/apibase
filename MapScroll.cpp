#include "MapScroll.h"
#include "Transform.h"
#include "Renderer.h";
#include "GameManager.h"
#include "Input.h"
#include "Image.h"
MapScroll::MapScroll()
{

}

MapScroll::~MapScroll()
{

}


void MapScroll::Init()
{
	Speed = 300.f;
	Renderer* renderer = GetComponent<Renderer>();
	ClientX	= GameManager::GetInstance()->GetRect().right;
	ClientY = GameManager::GetInstance()->GetRect().bottom;

	MapSizeX = renderer->GetImage()->GetWidth();
	MapSizeY = renderer->GetImage()->GetHeight();
}

void MapScroll::Update(float dt)
{
	float PosX = m_Transform->GetPosition().GetX();
	float PosY = m_Transform->GetPosition().GetY();

	if (Input::GetKey(VK_LEFT))
	{
		PosX += dt * Speed;
		if (PosX >= 0.f) PosX = 0.f;
	}
	if (Input::GetKey(VK_RIGHT))
	{
		PosX -= dt * Speed;
		if ( PosX <= -MapSizeX + ClientX )
		{
			PosX = -MapSizeX + ClientX ;
		}
	}
	if (Input::GetKey(VK_DOWN))
	{
		PosY -= dt * Speed;
		if (PosY <= -MapSizeY + ClientY)
		{
			PosY = -MapSizeY + ClientY;
		}
	}	

	if (Input::GetKey(VK_UP))
	{
		PosY += dt * Speed;
		if (PosY >= 0.f) PosY = 0.f;
	}

	m_Transform->SetPosition(Vector2(PosX, PosY));
}

void MapScroll::Release()
{

}