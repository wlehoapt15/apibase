#include "MapObject.h"
#include "GameObjectManager.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "ScrollScript.h"

MapObject::MapObject()
{

}

MapObject::~MapObject()
{

}

void MapObject::Init()
{
	//GameObject::Init();
	//AddComponent<ScrollS>();
	Renderer* renderer = AddComponent<Renderer>();
	renderer->SetImage(Image::GetImage("background1.bmp"));
	//m_Transform->SetAnchorPoint(Vector2(0.5f, 0.5f));
	ScrollScript* script = AddComponent<ScrollScript>();
	SetName("MapObject");
}


void MapObject::InitBackgroundImage(Image img) 
{
	/*m_BackgroundImg = img;
	*/InitBackgroundImage();
}

void MapObject::InitBackgroundImage() {
	//Renderer* renderer = GetComponent<Renderer>();
}

void MapObject::Release()
{

}