#include "PigMoveScript.h"
#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "Input.h"
#include "Animation.h"
#include "ImageAnimationClip.h"

PigMoveScript::PigMoveScript()
{

}

PigMoveScript::~PigMoveScript()
{

}

void PigMoveScript::Init()
{
	Renderer* renderer = GetComponent<Renderer>();
	Animation* ani = GetComponent<Animation>();

	Image* img = Image::GetImage("PigSprite.bmp");
	ImageAnimationClip* clip = new ImageAnimationClip(m_GameObject);
	clip->SetAnimationTime(1.f);
	clip->SetFrameCnt(3);
	clip->SetDirectionCnt(3);
	clip->SetExitTime(false);
	clip->SetLoop(true);
	clip->SetImage(img);
	clip->SetDirection(0.f);
	ani->Play(clip);
	renderer->SetImage(img);
}

void PigMoveScript::Update(float dt)
{
	if (Input::GetKey(VK_SPACE))
	{
		// addForce;
		
	}
}

void PigMoveScript::Release()
{
	
}