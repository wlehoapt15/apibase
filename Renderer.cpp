#include "Renderer.h"
#include "Image.h"
#include "Transform.h"
#include "GameManager.h"

Renderer::Renderer()
{
	m_Image = NULL;
	m_Alpha = 1.f;
	m_SizeX = 0;
	m_SizeY = 0;
	m_OffSetX = 0;
	m_OffSetY = 0;
}

Renderer::~Renderer()
{

}

void Renderer::Init()
{

}

void Renderer::Update(float dt)
{
	// 화면 밖에 넘어가는 조건??
	// 현재		좌표
	//Vector2 Position	= m_Transform->GetPosition();
	//// 윈도우	창크기
	//RECT	ClientRect  = GameManager::GetInstance()->GetRect();

	//if (Position.GetX() + (m_SizeX / 2  ) < 0 ||
	//	Position.GetX() - (m_SizeX / 2  ) > ClientRect.right ||
	//	Position.GetY() + (m_SizeY / 2 ) < 0 ||
	//	Position.GetY() - (m_SizeY / 2 ) > ClientRect.bottom )
	//{
	//	SetEnable(false);
	//}
	//else
	//{
	//	SetEnable(true);
	//}
}

void Renderer::Render(HDC hdc)
{
	if ( m_Image != NULL )
	{
		m_Image->SetAlpha(255 * m_Alpha);
		m_Image->Render( hdc, m_Transform, m_SizeX, m_SizeY, m_OffSetX, m_OffSetY );
	}
}

void Renderer::Release()
{

}

void Renderer::SetAlpha(float Alpha)
{
	m_Alpha = Alpha;
}

float Renderer::GetAlpha()
{
	return m_Alpha;
}

void Renderer::SetImage(Image* image)
{
	m_Image = image;
}

Image*	Renderer::GetImage()
{
	return m_Image;
}
