#include "SoundChannelInfo.h"

SoundChannelInfo::SoundChannelInfo()
{
	m_Channel = NULL;
}
// 채널간 사운드 비교 연산자
bool SoundChannelInfo::operator == (std::string SoundName)
{
	// 둘중에 하나라도 사운드 재생중이 아니라면
	if (m_PlayingSound.empty() || SoundName.empty() ){ return false; }
	if (m_PlayingSound != SoundName) return false;
	// 두 채널에서 재생중인 사운드가 동일하다
	return true;
}

bool SoundChannelInfo::operator == (SoundChannelInfo& Info)
{
	// 둘중에 하나라도 사운드 재생중이 아니라면
	if (m_PlayingSound.empty() || Info.m_PlayingSound.empty()) { return false; }
	if (m_PlayingSound != Info.m_PlayingSound ) return false;
	// 두 채널에서 재생중인 사운드가 동일하다
	return true;
}