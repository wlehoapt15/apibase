#ifndef _SCROLL_SCRIPT_H_
#define _SCROLL_SCRIPT_H_

#include "Component.h"

class ScrollScript : public Component
{
public:
	ScrollScript();
	~ScrollScript();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

private:
	float m_ScrollSpeed;

	float m_WinSizeX;
	float m_WinSizeY;

	int m_MapSizeX;
	int m_MapSizeY;

};

#endif
