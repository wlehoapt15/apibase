#include "EditMapObject.h"
#include "Renderer.h"
#include "Image.h"
#include "EditMapMoveScript.h"

EditMapObject::EditMapObject()
{

}

EditMapObject::~EditMapObject()
{

}

void EditMapObject::Init()
{
	AddComponent<EditMapMoveScript>();
	Renderer* renderer = AddComponent<Renderer>();
	renderer->SetImage(Image::GetImage("background1.bmp"));


}

void EditMapObject::Release()
{

}