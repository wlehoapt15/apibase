#ifndef _VECTOR2_H_
#define _VECTOR2_H_

#define PI 3.141592f
#define ANGLE_TO_RADIAN( ANGLE )  ( ( ( ANGLE ) * PI ) / 180.f )
#define RADIAN_TO_ANGLE( RADIAN ) ( ( (RADIAN) * 180.f ) / PI )

class Vector2
{
public:		// 생성자 소멸자
	Vector2();
	Vector2(float x, float y);
	Vector2(const Vector2& Val);

public:
	// 선형보간 , dt는 0~1사이의 값
	static Vector2 Lerp(Vector2 Start, Vector2 End, float dt)
	{
		return (Start * (1.0f - dt)) + (End * (dt));
	}

	// 베지어곡선
	static Vector2 BazierCurve(Vector2 Start, Vector2 Middle, Vector2 End, float dt)
	{
		Vector2 AB = Lerp(Start, Middle, dt);
		Vector2 BC = Lerp(Middle, End, dt);
		return Lerp(AB, BC, dt);
	}


public:		// 맴버 연산자
	Vector2  operator + (const Vector2& _Val);	// 백터의 덧셈연산
	Vector2  operator - (const Vector2& _Val);	// 백터의 뺼셈연산
	Vector2& operator += (const Vector2& _Val); // 벡터의 덧셈대입연산
	Vector2& operator -= (const Vector2& _Val); // 벡터의 뺄셈대입연산

	Vector2  operator *( float _Val);			// 벡터의 상수곱
	Vector2& operator *=( float _Val);			// 벡터의 상수 곱셈 대입
	Vector2  operator /(float _Val);			// 벡터의 나눗셈
	Vector2& operator /=(float _Val);			// 벡터의 상수 나눗셈 대입

public:		// 맴버 함수
	float	GetDistance();						// 벡터의 길이 구하기.
	float	GetDistance(const Vector2& _Val);	// 두벡터간의 거리 하기
	Vector2	Normalize();						// 단위벡터 만들기
	float	Dot(const Vector2 _Val);			// 벡터의 내적

public:		// 접근자 함수
	float GetX() { return m_x; }
	float GetY() { return m_y; }
	void  SetX(float x) { m_x = x; }
	void  SetY(float y) { m_y = y; }

public:
	float m_x;
	float m_y;
};

#endif