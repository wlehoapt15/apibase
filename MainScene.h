#ifndef _MAINSCENE_H_
#define _MAINSCENE_H_

#include "Scene.h"
#include "Bitmap.h"

class MainScene : public Scene
{
public:
	MainScene();
	~MainScene();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
			WPARAM wParam, LPARAM lParam);

public:
	Bitmap bit;
	HBITMAP hBit;
	bool	PointerDown;
};

#endif