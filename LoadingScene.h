#ifndef _LOADINGSCENE_H_
#define _LOADINGSCENE_H_

#include "Scene.h"

struct LoadSceneAsync
{
	Scene* NextScene;
	float  Progress;
};

DWORD WINAPI LoadingSceneThread(LPVOID Arg);

template  <typename SCENE>
class LoadingScene : public Scene
{
public:
	LoadingScene() { }
	virtual ~LoadingScene() { }

	// 순수 가상함수들
public:
	virtual void Init()				 = 0;
	virtual void Update(float dt)	 = 0;
	virtual void Render(HDC hdc)	 = 0;
	virtual void Release()			 = 0;

	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
	{
		return DefWindowProc(hWnd, iMessage, wParam, lParam);
	}

	void StartLoadingThread()
	{
		m_LoadSceneAsync.Progress  = 0.f;
		m_LoadSceneAsync.NextScene = dynamic_cast<Scene*>( new SCENE );
		if (m_LoadSceneAsync.NextScene == NULL)return;
		m_ThreadHandle = CreateThread(NULL, NULL,
			LoadingSceneThread, &m_LoadSceneAsync, NULL, NULL);
	}

	float GetProgress()   { return m_LoadSceneAsync.Progress;  }
	Scene* GetNextScene() { return m_LoadSceneAsync.NextScene; }

protected:
	LoadSceneAsync			m_LoadSceneAsync;
	HANDLE					m_ThreadHandle;
};

#endif
