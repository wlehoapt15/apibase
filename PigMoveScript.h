#ifndef _PIGMOVE_SCRIPT_H_
#define _PIGMOVE_SCRIPT_H_

#include "Component.h"

class PigMoveScript : public Component
{
public:
	PigMoveScript();
	~PigMoveScript();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Release();

private:
	
};

#endif