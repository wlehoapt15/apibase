#ifndef _HORIZONTALSCENE_H_
#define _HORIZONTALSCENE_H_

#include "Scene.h"

class HorizontalScene : public Scene
{
public:
	HorizontalScene();
	virtual ~HorizontalScene();

public:
	virtual void Init();
	virtual void Update(float dt);
	virtual void Render(HDC hdc);
	virtual void Release();
	virtual LRESULT SceneProc(HWND hWnd, UINT iMessage,
		WPARAM wParam, LPARAM lParam);
private:

};



#endif // _HORIZONTALSCENE_H_
